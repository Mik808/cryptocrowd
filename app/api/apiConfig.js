import {getFaucet} from "../branding";

export const blockTradesAPIs = {
    BASE: "",
    COINS_LIST: "",
    ACTIVE_WALLETS: "",
    TRADING_PAIRS: "",
    DEPOSIT_LIMIT: "",
    ESTIMATE_OUTPUT: "",
    ESTIMATE_INPUT: ""
};

export const openledgerAPIs = {
    BASE: "",
    COINS_LIST: "",
    ACTIVE_WALLETS: "",
    TRADING_PAIRS: "",
    DEPOSIT_LIMIT: "",
    ESTIMATE_OUTPUT: "",
    ESTIMATE_INPUT: "",
    RPC_URL: ""
};

export const rudexAPIs = {
    BASE: "",
    COINS_LIST: "",
    NEW_DEPOSIT_ADDRESS: ""
};

export const bitsparkAPIs = {
    BASE: "",
    COINS_LIST: "",
    ACTIVE_WALLETS: "",
    TRADING_PAIRS: "",
    DEPOSIT_LIMIT: "",
    ESTIMATE_OUTPUT: "",
    ESTIMATE_INPUT: ""
};

export const cryptoBridgeAPIs = {
    BASE: "",
    COINS_LIST: "",
    ACTIVE_WALLETS: "",
    MARKETS: "",
    TRADING_PAIRS: ""
};

export const citadelAPIs = {
    BASE: "",
    COINS_LIST: "",
    ACTIVE_WALLETS: "",
    TRADING_PAIRS: "",
    DEPOSIT_LIMIT: "",
    ESTIMATE_OUTPUT: "",
    ESTIMATE_INPUT: ""
};

export const gdex2APIs = {
    BASE: "",
    COINS_LIST: "",
    ACTIVE_WALLETS: "",
    TRADING_PAIRS: ""
};

// Legacy Deposit/Withdraw
export const gdexAPIs = {
    BASE: "",
    ASSET_LIST: "",
    ASSET_DETAIL: "",
    GET_DEPOSIT_ADDRESS: "",
    CHECK_WITHDRAY_ADDRESS: "",
    DEPOSIT_RECORD_LIST: "",
    DEPOSIT_RECORD_DETAIL: "",
    WITHDRAW_RECORD_LIST: "",
    WITHDRAW_RECORD_DETAIL: "",
    GET_USER_INFO: "",
    USER_AGREEMENT: "",
    WITHDRAW_RULE: ""
};

export const xbtsxAPIs = {
    BASE: "",
    COINS_LIST: ""
};

export const nodeRegions = [
    // region of the node follows roughly https://en.wikipedia.org/wiki/Subregion#/media/File:United_Nations_geographical_subregions.png
    "South America"
];

export const settingsAPIs = {
    // If you want a location to be translated, add the translation to settings in locale-xx.js
    // and use an object {translate: key} in WS_NODE_LIST
    url: "wss://crowdwiz.biz/ws",
    // url: "wss://newtestnet.crowdwiz.biz/ws",
    // url: "ws://build.onbc.xyz:11011",
    // DEFAULT_WS_NODE: "wss://newtestnet.crowdwiz.biz/ws",
    // DEFAULT_WS_NODE: "ws://build.onbc.xyz:11011",
    DEFAULT_WS_NODE: "wss://crowdwiz.biz/ws",
    WS_NODE_LIST: [
        {
            url: "wss://crowdwiz.biz/ws",
            // url: "wss://newtestnet.crowdwiz.biz/ws",
            // url: "ws://build.onbc.xyz:11011",
            location: "Europe"
        }
    ],
    DEFAULT_FAUCET: "https://crowdwiz.biz/faucet",
    TESTNET_FAUCET: "https://faucet.testnet.bitshares.eu"
};
