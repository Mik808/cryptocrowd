//manifest icons
require("file-loader?name=asset-symbols/app-icons/[name].png!./app-icons/48.png");
require("file-loader?name=asset-symbols/app-icons/[name].png!./app-icons/72.png");
require("file-loader?name=asset-symbols/app-icons/[name].png!./app-icons/96.png");
require("file-loader?name=asset-symbols/app-icons/[name].png!./app-icons/144.png");
require("file-loader?name=asset-symbols/app-icons/[name].png!./app-icons/180.png");
require("file-loader?name=asset-symbols/app-icons/[name].png!./app-icons/192.png");
require("file-loader?name=asset-symbols/app-icons/[name].png!./app-icons/apple-touch-icon.png");

//GameZone
require("file-loader?name=asset-symbols/gamezone/[name].png!./gamezone/heads.png");
require("file-loader?name=asset-symbols/gamezone/[name].png!./gamezone/dice.png");
require("file-loader?name=asset-symbols/gamezone/[name].png!./gamezone/auction.png");
require("file-loader?name=asset-symbols/gamezone/[name].png!./gamezone/lottery.png");

//CrowdMarket
require("file-loader?name=asset-symbols/gamezone/[name].png!./gamezone/megaphone.png");
require("file-loader?name=asset-symbols/gamezone/[name].png!./gamezone/money-box.png");

//Matrix
require("file-loader?name=asset-symbols/gamezone/[name].png!./gamezone/matrix.png");
require("file-loader?name=asset-symbols/matrix/[name].png!./matrix/matrix_bg.png");
require("file-loader?name=asset-symbols/matrix/[name].png!./matrix/matrix_bg--mobile.png");
require("file-loader?name=asset-symbols/matrix/[name].png!./matrix/intro_bg.png");
require("file-loader?name=asset-symbols/matrix/[name].png!./matrix/indicator_bg.png");

//OTHER
require("file-loader?name=asset-symbols/index/other/[name].png!./index/other/clouds.png");
require("file-loader?name=asset-symbols/index/other/[name].png!./index/other/speedometer.png");

// INDEX HEADER
require("file-loader?name=asset-symbols/index/header/[name].png!./index/header/land-bg-christmas.png");
require("file-loader?name=asset-symbols/index/header/[name].png!./index/header/land-bg-christmas-tablet.png");
require("file-loader?name=asset-symbols/index/header/[name].png!./index/header/land-bg-christmas-mob.png");
