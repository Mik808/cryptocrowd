import React from "react";
import Translate from "react-translate-component";
import Icon from "../Icon/Icon";
import WalletDb from "stores/WalletDb";
import WalletUnlockActions from "actions/WalletUnlockActions";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import {ChainStore} from "bitsharesjs";
import {connect} from "alt-react";

let sendMessageIcon = require("assets/icons/send-message.png");

class SendMessage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showModal: false,
            currentAccount: this.props.currentAccount,
            isDisabled: true
        };

        this.closeModal = this.closeModal.bind(this);
        this.openModal = this.openModal.bind(this);

        this.popupValidate = this.popupValidate.bind(this);
        this.isLocked = this.isLocked.bind(this);
    }

    closeModal() {
        this.setState({
            showModal: false
        });
    }

    openModal() {
        this.setState({
            showModal: true
        });
    }

    popupValidate() {
        let messageText = document.getElementById("messageText").value;

        if (messageText.length >= 1) {
            this.setState({
                isDisabled: false
            });
        } else {
            this.setState({
                isDisabled: true
            });
        }
    }

    isLocked() {
        if (this.state.currentAccount) {
            let fromAccount = this.props.currentAccount.get("id");
            let toAccount = this.props.toAccountId;
            let messageText = document.getElementById("messageText").value;

            if (WalletDb.isLocked()) {
                WalletUnlockActions.unlock()
                    .then(() => {
                        AccountActions.tryToSetCurrentAccount();
                        AccountActions.sendMessage(
                            fromAccount,
                            toAccount,
                            messageText
                        );
                        this.closeModal();
                    })
                    .catch(() => {});
            } else {
                AccountActions.tryToSetCurrentAccount();
                AccountActions.sendMessage(fromAccount, toAccount, messageText);
                this.closeModal();
            }
        }
    }

    sendMessage() {}

    render() {
        let showModal = this.state.showModal;
        let toAccount = this.props.toAccount;
        let width = window.innerWidth;

        return (
            <div className="send-message__container">
                <div className="send-message__inner">
                    <Translate
                        className="send-message__text"
                        content="send_message.account_text"
                    />
                    {""}
                    <span className="send-message__text send-message__text--name">
                        {toAccount}
                    </span>
                </div>

                <span
                    className="send-message__btn"
                    onClick={this.openModal.bind()}
                >
                    <Translate
                        className="send-message__acc-name"
                        content="send_message.message_btn"
                    />

                    <img src={sendMessageIcon} alt="" />
                </span>

                {/* MESSAGE POPUP */}
                {showModal ? (
                    <div>
                        <div className="send-message-popup">
                            <Icon
                                size="1x"
                                name="cross"
                                className="send-message-popup__modal-close"
                                onClick={this.closeModal.bind()}
                            />

                            <div className="send-message-popup__inner">
                                <Translate
                                    className="send-message-popup__text"
                                    content="send_message.message_text"
                                />

                                <textarea
                                    className="send-message-popup__field"
                                    type="text"
                                    name="messageText"
                                    id="messageText"
                                    onChange={this.popupValidate.bind()}
                                    rows={width > 576 ? "10" : "17"}
                                />
                            </div>

                            <button
                                className="send-message-popup__btn"
                                type="button"
                                onClick={this.isLocked.bind()}
                                disabled={this.state.isDisabled}
                            >
                                <Translate content="send_message.send_btn" />
                            </button>
                        </div>
                        {/* OVERLAY */}
                        <div className="send-message-popup__overlay"></div>
                    </div>
                ) : null}
            </div>
        );
    }
}

export default SendMessage = connect(SendMessage, {
    listenTo() {
        return [AccountStore];
    },
    getProps() {
        if (AccountStore.getState().passwordAccount) {
            return {
                currentAccount: ChainStore.fetchFullAccount(
                    AccountStore.getState().passwordAccount
                )
            };
        }
    }
});
