import React from "react";
import Translate from "react-translate-component";
import {Apis} from "bitsharesjs-ws";
import AccountActions from "actions/AccountActions";
import Icon from "../Icon/Icon";
import counterpart from "counterpart";
import VestingBalance from "./VestingBalance";
import TranslateWithLinks from "../Utility/TranslateWithLinks";
import {Tabs, Tab} from "../Utility/Tabs";
import InvestTransfer from "../Investment/components/InvestTransfer";

require("./scss/vesting.scss");

class AccountVesting extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            vbs: null,
            showClipboardMsg: false,
            currentAccount: null,
            silverBalance: 0
        };
    }
    componentDidMount() {
        if (this.props.account) {
            let balance_id = this.props.account.getIn(["balances", "1.3.4"]);
            let userName = this.props.account.get("name");

            this.setState({
                currentAccount: userName
            });

            Apis.instance()
                .db_api()
                .exec("get_objects", [[balance_id]])
                .then(date => {
                    this.setState({
                        silverBalance: date[0]["balance"] / 100000
                    });
                });
        } else {
            this.props.history.push("/create-account/password");
        }
    }

    UNSAFE_componentWillMount() {
        this.retrieveVestingBalances.call(this, this.props.account.get("id"));
    }

    UNSAFE_componentWillUpdate(nextProps) {
        let newId = nextProps.account.get("id");
        let oldId = this.props.account.get("id");

        if (newId !== oldId) {
            this.retrieveVestingBalances.call(this, newId);
        }
    }

    retrieveVestingBalances(accountId) {
        accountId = accountId || this.props.account.get("id");
        Apis.instance()
            .db_api()
            .exec("get_vesting_balances", [accountId])
            .then(vbs => {
                this.setState({vbs});
            })
            .catch(err => {
                console.log("error:", err);
            });
    }

    upgradeStatusAccount(id, statusType, e) {
        e.preventDefault();
        AccountActions.upgradeStatusAccount(id, statusType);
    }

    async selectValue(ref) {
        try {
            await navigator.clipboard.writeText(ref);
            this.setState({["showClipboardMsg"]: true});
            setTimeout(() => {
                this.setState({["showClipboardMsg"]: false});
            }, 2000);
        } catch (err) {
            console.log(err, "text is not copied to clipboard");
        }
    }

    render() {
        let {vbs} = this.state;

        if (
            !vbs ||
            !this.props.account ||
            !this.props.account.get("vesting_balances")
        ) {
            return null;
        }

        let width = window.innerWidth;
        const tablet = 768;

        let account = this.props.account.toJS();
        let account_name = this.props.account.get("name");
        let referalStatus = this.props.account.get("referral_status_type");
        let statuses = [
            counterpart.translate("account.structure.client_status"),
            "Start",
            "Expert",
            "Citizen",
            "infinity"
        ];
        let statusExpirationDateRaw = this.props.account.get(
            "referral_status_expiration_date"
        );

        if (statusExpirationDateRaw) {
            var myStdate = new Date(statusExpirationDateRaw + "Z");
            var statusExpirationDate = myStdate.toLocaleString();
        }

        let total_vesting_amount = 0;

        vbs.map(vb => (total_vesting_amount += vb["balance"]["amount"]));

        // table head for vesting item
        let tableHead = (
            <div className="vesting__head">
                <Translate component="div" content="account.vesting.balance" />
                <Translate
                    component="div"
                    content="account.vesting.total-deposit"
                />
                <Translate
                    component="div"
                    content="account.vesting.available"
                />
                <Translate
                    component="div"
                    content="account.vesting.withdrawed"
                />
            </div>
        );

        let currentAccount = this.state.currentAccount;
        let silverBalance = this.state.silverBalance.toString();

        return (
            <div ref="appTables" className="cwd-common__wrap">
                <Translate
                    className="component-subheader"
                    content="account.vesting.status_header"
                />

                <div className="contract__info-block-wrap">
                    <div className="contract__data-row">
                        <Translate
                            className="contract__text"
                            content="account.vesting.active-contract"
                        />

                        <div className="contract__text contract__text--data">
                            {statuses[referalStatus]}
                        </div>
                    </div>
                    {referalStatus == 4 ? null : (
                        <div className="contract__data-row">
                            <Translate
                                className="contract__text"
                                content="account.vesting.update-account"
                            />

                            <div className="contract__update-btn-wrap">
                                {referalStatus == 1 ||
                                referalStatus == 2 ||
                                referalStatus == 3 ||
                                referalStatus == 4 ? null : (
                                    <button
                                        className="contract__btn contract__btn--start"
                                        onClick={this.upgradeStatusAccount.bind(
                                            this,
                                            account.id,
                                            1
                                        )}
                                    >
                                        <Translate content="account.vesting.status1" />
                                    </button>
                                )}
                                {referalStatus == 2 ||
                                referalStatus == 3 ||
                                referalStatus == 4 ? null : (
                                    <button
                                        className="contract__btn contract__btn--expert"
                                        onClick={this.upgradeStatusAccount.bind(
                                            this,
                                            account.id,
                                            2
                                        )}
                                    >
                                        <Translate content="account.vesting.status2" />
                                    </button>
                                )}
                                {referalStatus == 3 ||
                                referalStatus == 4 ? null : (
                                    <button
                                        className="contract__btn contract__btn--citizen"
                                        onClick={this.upgradeStatusAccount.bind(
                                            this,
                                            account.id,
                                            3
                                        )}
                                    >
                                        <Translate content="account.vesting.status3" />
                                    </button>
                                )}
                                {referalStatus == 4 ? null : (
                                    <button
                                        className="contract__btn contract__btn--citizen"
                                        onClick={this.upgradeStatusAccount.bind(
                                            this,
                                            account.id,
                                            4
                                        )}
                                    >
                                        <Translate content="account.vesting.status4" />
                                    </button>
                                )}
                            </div>
                        </div>
                    )}
                    {/* contract__update-wrapper end */}
                    <div className="contract__data-row">
                        <Translate
                            content="account.vesting.ref_link"
                            className="contract__text contract__text--mobile"
                        />

                        <div
                            className="contract__ref"
                            onClick={() =>
                                this.selectValue(
                                    `https://crowdwiz.biz/?r=${account_name}`
                                )
                            }
                        >
                            <div className="contract__text contract__text--ref-link">{`https://crowdwiz.biz/?r=${account_name}`}</div>
                            <Icon size="1_5x" name="copy" />
                            {this.state.showClipboardMsg ? (
                                <div className="contract__msg">
                                    <Translate content="account.vesting.copy_text" />
                                </div>
                            ) : null}
                        </div>
                    </div>
                    {!referalStatus == 0 ? (
                        <div className="contract__data-row">
                            <Translate
                                className="contract__text"
                                content="account.vesting.active_before"
                            />

                            <div className="contract__text contract__text--data">
                                {statusExpirationDate}&nbsp;(GMT)
                            </div>

                            <button
                                className="contract__btn contract__btn--upgrade"
                                onClick={this.upgradeStatusAccount.bind(
                                    this,
                                    account.id,
                                    this.props.account.get(
                                        "referral_status_type"
                                    )
                                )}
                            >
                                <Translate content="account.vesting.extend" />
                            </button>
                        </div>
                    ) : null}
                </div>

                <Tabs
                    className="cwd-tabs"
                    tabsClass="cwd-tabs__list"
                    contentClass="cwd-tabs__content"
                    segmented={false}
                    actionButtons={false}
                >
                    <Tab title="account.vesting.vesting_header">
                        {/*Vesting items*/}
                        <div className="contract__vesting-wrap">
                            {!vbs.length || total_vesting_amount == 0 ? (
                                <p className="contract__info-text">
                                    <TranslateWithLinks
                                        string="account.vesting.no_balances"
                                        keys={[
                                            {
                                                type: "account",
                                                value: account_name,
                                                arg: "account"
                                            }
                                        ]}
                                    />
                                </p>
                            ) : (
                                <div>
                                    {width > tablet && tableHead}
                                    <div className="vesting__container">
                                        {vbs.map(vb => (
                                            <VestingBalance
                                                key={vb.id}
                                                vb={vb}
                                                account={account}
                                                handleChanged={this.retrieveVestingBalances.bind(
                                                    this
                                                )}
                                                notAvailable={
                                                    vb.policy[1]
                                                        .vesting_duration_seconds ==
                                                        0 && vb.policy[0] == 0
                                                }
                                            />
                                        ))}
                                    </div>
                                </div>
                            )}
                        </div>
                    </Tab>

                    <Tab title="invest.tab_04">
                        <InvestTransfer
                            key="infinity4"
                            currentAccount={currentAccount}
                            toName="infinity-withdraw"
                            percent="0"
                            minDeposit={silverBalance}
                            transferAsset="1.3.4"
                        />
                    </Tab>
                </Tabs>
            </div>
        );
    }
}

// AccountVesting.VestingBalance = VestingBalance;
export default AccountVesting;
