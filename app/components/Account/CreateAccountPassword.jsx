import React from "react";
import {connect} from "alt-react";
import classNames from "classnames";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import AccountNameInput from "./../Forms/AccountNameInput";
import WalletDb from "stores/WalletDb";
import {Link} from "react-router-dom";
import AccountSelect from "../Forms/AccountSelect";
import TransactionConfirmStore from "stores/TransactionConfirmStore";
import LoadingIndicator from "../LoadingIndicator";
import Translate from "react-translate-component";
import counterpart from "counterpart";
import {ChainStore, FetchChain, key} from "bitsharesjs";
import ReactTooltip from "react-tooltip";
import utils from "common/utils";
import SettingsActions from "actions/SettingsActions";
import WalletUnlockActions from "actions/WalletUnlockActions";
import Icon from "../Icon/Icon";
import CopyButton from "../Utility/CopyButton";
import {withRouter} from "react-router-dom";
import {scroller} from "react-scroll";
import {Notification, Tooltip} from "crowdwiz-ui-modal";
import AccountSelector from "../Account/AccountSelector";
import ls from "common/localStorage";

let accountStorage = new ls("__graphene__");

class CreateAccountPassword extends React.Component {
    constructor() {
        super();
        this.state = {
            validAccountName: false,
            account: null,
            accountName: "",
            referralAccount: "",
            accountNameRef: "",
            isReferralAccount: false,
            from_error: null,
            error: null,
            validPassword: false,
            registrar_account: null,
            loading: false,
            hide_refcode: true,
            show_identicon: false,
            step: 1,
            showPass: false,
            generatedPassword: ("P" + key.get_random_key().toWif()).substr(
                0,
                45
            ),
            confirm_password: "",
            understand_1: false,
            understand_2: false,
            understand_3: false,
            showInputPass: "password"
        };

        this.account_input = React.createRef();

        this.onFinishConfirm = this.onFinishConfirm.bind(this);

        this.scrollToInput = this.scrollToInput.bind(this);
    }

    UNSAFE_componentWillMount() {
        if (!WalletDb.getWallet()) {
            SettingsActions.changeSetting({
                setting: "passwordLogin",
                value: true
            });
        }
    }

    componentDidMount() {
        ReactTooltip.rebuild();
        this.scrollToInput();

        if (AccountStore.getState().referralAccount) {
            this.setState({
                referralAccount: AccountStore.getState().referralAccount
            });
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !utils.are_equal_shallow(nextState, this.state);
    }

    scrollToInput() {
        scroller.scrollTo(`scrollToInput`, {
            duration: 1500,
            delay: 100,
            smooth: true,
            containerId: "accountForm"
        });
    }

    isValid() {
        let firstAccount = AccountStore.getMyAccounts().length === 0;
        let valid = this.state.validAccountName;

        if (!WalletDb.getWallet()) {
            valid = valid && this.state.validPassword;
        }
        if (!firstAccount) {
            valid = valid && this.state.registrar_account;
        }
        return (
            valid &&
            // referralAccount &&
            this.state.understand_1 &&
            this.state.understand_2
        );
    }

    onAccountNameChange(e) {
        const state = {};
        if (e.valid !== undefined) state.validAccountName = e.valid;
        if (e.value !== undefined) state.accountName = e.value;
        if (!this.state.show_identicon) state.show_identicon = true;
        this.setState(state);
    }

    onFinishConfirm(confirm_store_state) {
        if (
            confirm_store_state.included &&
            confirm_store_state.broadcasted_transaction
        ) {
            TransactionConfirmStore.unlisten(this.onFinishConfirm);
            TransactionConfirmStore.reset();

            FetchChain("getAccount", this.state.accountName, undefined, {
                [this.state.accountName]: true
            }).then(() => {
                this.props.history.push(
                    "/wallet/backup/create?newAccount=true"
                );
            });
        }
    }

    _unlockAccount(name, password) {
        SettingsActions.changeSetting({
            setting: "passwordLogin",
            value: true
        });

        WalletDb.validatePassword(password, true, name);
        WalletUnlockActions.checkLock.defer();
    }

    createAccount(name, password) {
        let refcode = this.refs.refcode ? this.refs.refcode.value() : null;
        let referralAccount = accountStorage.get("referralAccount");
        this.setState({loading: true});

        AccountActions.createAccountWithPassword(
            name,
            password,
            this.state.registrar_account,
            referralAccount || this.state.registrar_account,
            0,
            refcode
        )
            .then(() => {
                AccountActions.setPasswordAccount(name);
                // User registering his own account
                if (this.state.registrar_account) {
                    FetchChain("getAccount", name, undefined, {
                        [name]: true
                    }).then(() => {
                        this.setState({
                            step: 2,
                            loading: false
                        });
                        this._unlockAccount(name, password);
                    });
                    TransactionConfirmStore.listen(this.onFinishConfirm);
                } else {
                    // Account registered by the faucet
                    FetchChain("getAccount", name, undefined, {
                        [name]: true
                    }).then(() => {
                        this.setState({
                            step: 2
                        });
                        this._unlockAccount(name, password);
                    });
                }
            })
            .catch(error => {
                console.log("ERROR AccountActions.createAccount", error);
                let error_msg =
                    error.base && error.base.length && error.base.length > 0
                        ? error.base[0]
                        : "unknown error";
                if (error.remote_ip) error_msg = error.remote_ip[0];

                Notification.error({
                    message: counterpart.translate(
                        "notifications.account_create_failure",
                        {
                            account_name: name,
                            error_msg: error_msg
                        }
                    )
                });

                this.setState({loading: false});
            });
    }

    onSubmit(e) {
        e.preventDefault();
        if (!this.isValid()) return;
        let account_name = this.accountNameInput.getValue();
        let password = this.state.generatedPassword;
        this.createAccount(account_name, password);
    }

    onRegistrarAccountChange(registrar_account) {
        this.setState({registrar_account});
    }

    _onInput(value, e) {
        this.setState({
            [value]:
                value === "confirm_password"
                    ? e.target.value
                    : !this.state[value],
            validPassword:
                value === "confirm_password"
                    ? e.target.value === this.state.generatedPassword
                    : this.state.validPassword
        });
    }

    handleAccountNameChange = accountName =>
        this.setState({accountName, error: null});

    accountChanged(accountNameRef) {
        if (!accountNameRef) this.setState({account: null});
        this.setState({accountNameRef, error: null});
    }

    onAccountChanged(account) {
        this.setState({account, error: null, isReferralAccount: true});
        accountStorage.set("referralAccount", account.get("name"));
    }

    _renderAccountCreateForm() {
        let {registrar_account} = this.state;

        let my_accounts = AccountStore.getMyAccounts();
        let firstAccount = my_accounts.length === 0;
        let valid = this.isValid();
        let isLTM = false;
        let registrar = registrar_account
            ? ChainStore.getAccount(registrar_account)
            : null;
        if (registrar) {
            if (registrar.get("lifetime_referrer") == registrar.get("id")) {
                isLTM = true;
            }
        }

        let buttonClass = classNames("submit-button button no-margin", {
            disabled: !valid || (registrar_account && !isLTM)
        });

        let referralAccount = AccountStore.getState().referralAccount;
        null;

        let tabIndex = 1;

        return (
            <div style={{textAlign: "left"}}>
                <form
                    style={{maxWidth: "60rem"}}
                    onSubmit={this.onSubmit.bind(this)}
                    noValidate
                >
                    {/* {referralInput} */}

                    {this.state.referralAccount ? (
                        <div id="referralAccount">
                            <Translate
                                content="wallet.user_referer"
                                component="p"
                            />
                            <AccountSelector
                                ref="account_input"
                                accountName={this.state.referralAccount}
                                onChange={this.accountChanged.bind(this)}
                                onAccountChanged={this.onAccountChanged.bind(
                                    this
                                )}
                                account={this.state.referralAccount}
                                error={this.state.from_error}
                                tabIndex={tabIndex++}
                                id="landingFormRefLogin"
                            />
                        </div>
                    ) : (
                        <div id="referralAccount">
                            <Translate
                                content="wallet.user_referer"
                                component="p"
                            />
                            <AccountSelector
                                ref="account_input"
                                accountName={this.state.accountNameRef}
                                onChange={this.accountChanged.bind(this)}
                                onAccountChanged={this.onAccountChanged.bind(
                                    this
                                )}
                                account={this.state.accountNameRef}
                                error={this.state.from_error}
                                tabIndex={tabIndex++}
                                id="landingFormRefLogin"
                            />
                        </div>
                    )}

                    <AccountNameInput
                        ref={ref => {
                            if (ref) {
                                this.accountNameInput = ref.refs.nameInput;
                            }
                        }}
                        cheapNameOnly={!!firstAccount}
                        onChange={this.onAccountNameChange.bind(this)}
                        accountShouldNotExist={true}
                        placeholder={counterpart.translate(
                            "wallet.account_public"
                        )}
                        noLabel
                    />

                    <section className="form-group">
                        <label className="left-label">
                            <Translate content="wallet.generated" />
                            &nbsp;&nbsp;
                            <Tooltip
                                title={counterpart.translate(
                                    "tooltip.generate"
                                )}
                            >
                                <span className="tooltip">
                                    <Icon
                                        name="question-circle"
                                        title="icons.question_circle"
                                    />
                                </span>
                            </Tooltip>
                        </label>
                        <div style={{paddingBottom: "0.5rem"}}>
                            <span className="inline-label auth-textarea-wrap">
                                <textarea
                                    style={{
                                        marginBottom: "0px",
                                        marginRight: "10px"
                                    }}
                                    rows="2"
                                    readOnly
                                    value={this.state.generatedPassword}
                                    disabled
                                />
                                <CopyButton
                                    text={this.state.generatedPassword}
                                    tip="tooltip.copy_password"
                                    dataPlace="top"
                                />
                            </span>
                        </div>
                    </section>

                    <section>
                        <label className="left-label">
                            <Translate content="wallet.confirm_password" />
                        </label>
                        <input
                            type="password"
                            name="password"
                            id="password"
                            value={this.state.confirm_password}
                            onChange={this._onInput.bind(
                                this,
                                "confirm_password"
                            )}
                        />
                        {this.state.confirm_password &&
                        this.state.confirm_password !==
                            this.state.generatedPassword ? (
                            <div className="has-error">
                                <Translate content="wallet.confirm_error" />
                            </div>
                        ) : null}
                    </section>

                    <br />

                    <div
                        className="confirm-checks"
                        onClick={this._onInput.bind(this, "understand_3")}
                    >
                        <label
                            htmlFor="checkbox-1"
                            className="auth-agree-label"
                        >
                            <input
                                type="checkbox"
                                id="checkbox-1"
                                onChange={() => {}}
                                checked={this.state.understand_3}
                                className="auth-checkbox"
                            />
                            <div className="auth-agree-text">
                                <Translate content="wallet.understand_3" />
                            </div>
                        </label>
                    </div>
                    <br />
                    <div
                        className="confirm-checks"
                        onClick={this._onInput.bind(this, "understand_1")}
                    >
                        <label
                            className="auth-agree-label"
                            htmlFor="checkbox-2"
                        >
                            <input
                                type="checkbox"
                                id="checkbox-2"
                                onChange={() => {}}
                                checked={this.state.understand_1}
                                className="auth-checkbox"
                            />
                            <div className="auth-agree-text">
                                <Translate content="wallet.understand_1" />
                            </div>
                        </label>
                    </div>
                    <br />

                    <div
                        className="confirm-checks"
                        style={{paddingBottom: "1.5rem"}}
                        onClick={this._onInput.bind(this, "understand_2")}
                    >
                        <label
                            className="auth-agree-label"
                            htmlFor="checkbox-3"
                        >
                            <input
                                type="checkbox"
                                id="checkbox-3"
                                onChange={() => {}}
                                checked={this.state.understand_2}
                                className="auth-checkbox"
                            />
                            <div className="auth-agree-text">
                                <Translate content="wallet.understand_2" />
                            </div>
                        </label>
                    </div>
                    {/* If this is not the first account, show dropdown for fee payment account */}
                    {firstAccount ? null : (
                        <div
                            className="full-width-content form-group no-overflow"
                            style={{paddingTop: 30}}
                        >
                            <label>
                                <Translate content="account.pay_from" />
                            </label>
                            <AccountSelect
                                account_names={my_accounts}
                                onChange={this.onRegistrarAccountChange.bind(
                                    this
                                )}
                            />
                            {registrar_account && !isLTM ? (
                                <div className="facolor-error">
                                    <Translate
                                        component="span"
                                        content="wallet.must_be_ltm"
                                    />
                                </div>
                            ) : null}
                        </div>
                    )}

                    {/* Submit button */}
                    {this.state.loading ? (
                        <LoadingIndicator type="three-bounce" />
                    ) : (
                        <button style={{width: "100%"}} className={buttonClass}>
                            <Translate content="account.create_account" />
                        </button>
                    )}
                </form>
            </div>
        );
    }

    _renderAccountCreateText() {
        let my_accounts = AccountStore.getMyAccounts();
        let firstAccount = my_accounts.length === 0;

        return (
            <div>
                <h4
                    style={{
                        fontWeight: "normal",
                        fontFamily: "Roboto-Medium, arial, sans-serif",
                        fontStyle: "normal",
                        paddingBottom: 15
                    }}
                >
                    <Translate content="wallet.wallet_password" />
                </h4>

                <Translate
                    style={{textAlign: "left"}}
                    unsafe
                    component="p"
                    content="wallet.create_account_password_text"
                />

                <Translate
                    style={{textAlign: "left"}}
                    component="p"
                    content="wallet.create_account_text"
                />

                {firstAccount ? null : (
                    <Translate
                        style={{textAlign: "left"}}
                        component="p"
                        content="wallet.not_first_account"
                    />
                )}
            </div>
        );
    }

    _renderBackup() {
        return (
            <div className="backup-submit">
                <p>
                    <Translate unsafe content="wallet.password_crucial" />
                </p>

                <div className="gen-pass">
                    <label>
                        <Translate content="wallet.your_pass" />
                    </label>
                    <div className="gen-pass__input-wrap">
                        <div className="gen-pass__input-inner">
                            <input
                                type={this.state.showInputPass}
                                value={this.state.generatedPassword}
                                className="gen-pass__pass"
                            />
                            <div
                                onClick={e => this.showInputPass(e)}
                                className="gen-pass__show-pass"
                            >
                                <Icon name="eye-pass" />
                            </div>
                        </div>
                        <CopyButton
                            text={this.state.generatedPassword}
                            tip="tooltip.copy_password"
                            dataPlace="top"
                            className="gen-pass__copy"
                        />
                    </div>
                </div>

                <p
                    className="txtlabel warning float-left"
                    style={{marginTop: "20px"}}
                >
                    <Translate unsafe content="wallet.password_lose_warning" />
                </p>

                <div
                    onClick={() => {
                        this.props.history.push(
                            `/account/${this.state.accountName}`
                        );
                    }}
                    className="button float-left"
                >
                    <Translate content="wallet.ok_done" />
                </div>
            </div>
        );
    }

    _renderGetStarted() {
        return (
            <div>
                <table className="table">
                    <tbody>
                        <tr>
                            <td>
                                <Translate content="wallet.tips_dashboard" />:
                            </td>
                            <td>
                                <Link to="/">
                                    <Translate content="header.dashboard" />
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_account" />:
                            </td>
                            <td>
                                <Link
                                    to={`/account/${this.state.accountName}/overview`}
                                >
                                    <Translate content="wallet.link_account" />
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_deposit" />:
                            </td>
                            <td>
                                <Link to="/deposit-withdraw">
                                    <Translate content="wallet.link_deposit" />
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_transfer" />:
                            </td>
                            <td>
                                <Link to="/transfer">
                                    <Translate content="wallet.link_transfer" />
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_settings" />:
                            </td>
                            <td>
                                <Link to="/settings">
                                    <Translate content="header.settings" />
                                </Link>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }

    _renderGetStartedText() {
        return (
            <div>
                <p
                    style={{
                        fontWeight: "normal",
                        fontFamily: "Roboto-Medium, arial, sans-serif",
                        fontStyle: "normal"
                    }}
                >
                    <Translate content="wallet.congrat" />
                </p>

                <p>
                    <Translate content="wallet.tips_explore_pass" />
                </p>

                <p>
                    <Translate content="wallet.tips_header" />
                </p>

                <p className="txtlabel warning">
                    <Translate content="wallet.tips_login" />
                </p>
            </div>
        );
    }

    showInputPass() {
        if (this.state.showInputPass === "password") {
            this.setState({showInputPass: "text"});
        } else {
            this.setState({showInputPass: "password"});
        }
    }

    render() {
        let {step} = this.state;

        return (
            <div
                className="sub-content"
                id="scrollToInput"
                name="scrollToInput"
            >
                <div>
                    {step === 2 ? (
                        <div className="registration-step">
                            <div className="registration-step__inner">
                                <Translate
                                    content="wallet.step"
                                    component="div"
                                    className="registration-step__step"
                                />
                                <div>
                                    <span> 2</span>
                                    <span className="registration-step__grey-span">
                                        /2
                                    </span>
                                </div>
                            </div>

                            <Translate
                                content={"wallet.step_" + step + "_text"}
                                component="div"
                            />
                        </div>
                    ) : null}

                    {step === 3 ? this._renderGetStartedText() : null}

                    {step === 1 ? (
                        <div>{this._renderAccountCreateForm()}</div>
                    ) : step === 2 ? (
                        this._renderBackup()
                    ) : (
                        this._renderGetStarted()
                    )}
                </div>
            </div>
        );
    }
}

CreateAccountPassword = withRouter(CreateAccountPassword);

export default connect(CreateAccountPassword, {
    listenTo() {
        return [AccountStore];
    },
    getProps() {
        return {};
    }
});
