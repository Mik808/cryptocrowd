import React from "react";
import {Link} from "react-router-dom";
import {connect} from "alt-react";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import SettingsStore from "stores/SettingsStore";
import SettingsActions from "actions/SettingsActions";
import Icon from "../Icon/Icon";
import Translate from "react-translate-component";
import WalletDb from "stores/WalletDb";
import WalletUnlockStore from "stores/WalletUnlockStore";
import WalletUnlockActions from "actions/WalletUnlockActions";
import WalletManagerStore from "stores/WalletManagerStore";
import cnames from "classnames";
import TotalBalanceValue from "../Utility/TotalBalanceValue";
import ReactTooltip from "react-tooltip";
import {Apis} from "bitsharesjs-ws";
import {ChainStore} from "bitsharesjs";
import {List} from "immutable";
import DropDownMenu from "./HeaderDropdown";
import {withRouter} from "react-router-dom";
import {setLocalStorageType, isPersistantType} from "lib/common/localStorage";
import MobileMenu from "./MobileMenu";
import Footer from "../Layout/Footer";

import {getLogo} from "branding";
var logo = getLogo();

var start = require("assets/icons/start-contract.png");
var expert = require("assets/icons/expert-contract.png");
var citizen = require("assets/icons/citizen-contract.png");
var infinity = require("assets/icons/infinity-contract.png");
var client = require("assets/icons/client-contract.png");

class Header extends React.Component {
    constructor(props) {
        super();
        this.state = {
            active: props.location.pathname,
            dropdownActive: false,
            mobileMenuActive: false,
            status: 0,
            synced: this._syncStatus()
        };

        this.unlisten = null;
        this._toggleDropdownMenu = this._toggleDropdownMenu.bind(this);

        this._closeDropdown = this._closeDropdown.bind(this);

        this.onBodyClick = this.onBodyClick.bind(this);
        this.getUserId = this.getUserId.bind(this);

        this._syncStatus = this._syncStatus.bind(this);
    }

    componentWillMount() {
        this.unlisten = this.props.history.listen(newState => {
            if (this.unlisten && this.state.active !== newState.pathname) {
                this.setState({
                    active: newState.pathname
                });
            }
        });
    }

    componentDidMount() {
        setTimeout(() => {
            ReactTooltip.rebuild();
        }, 1250);

        this.syncCheckInterval = setInterval(
            this._syncStatus.bind(this, true),
            5000
        );

        let mainContainer = document.getElementById("mainContainer");

        mainContainer.addEventListener("click", this.onBodyClick, {
            dropdownActive: false
        });
    }

    _syncStatus(setState = false) {
        let synced = this.getBlockTimeDelta() < 5;
        if (setState && synced !== this.state.synced) {
            this.setState({synced});
        }
        return synced;
    }

    getBlockTimeDelta() {
        try {
            let bt =
                (this.getBlockTime().getTime() +
                    ChainStore.getEstimatedChainTimeOffset()) /
                1000;
            let now = new Date().getTime() / 1000;
            return Math.abs(now - bt);
        } catch (err) {
            return -1;
        }
    }

    async getUserId(account_name) {
        if (account_name != null) {
            let user_id = await Apis.instance()
                .db_api()
                .exec("get_full_accounts", [[account_name], false]);
            if (
                this.state.status !=
                user_id[0][1]["account"]["referral_status_type"]
            ) {
                this.setState({
                    status: user_id[0][1]["account"]["referral_status_type"]
                });
            }
        }
    }

    componentWillUnmount() {
        if (this.unlisten) {
            this.unlisten();
            this.unlisten = null;
        }
        document.body.removeEventListener("click", this.onBodyClick);
        clearInterval(this.syncCheckInterval);
    }

    _toggleLock(e) {
        e.preventDefault();
        if (WalletDb.isLocked()) {
            WalletUnlockActions.unlock()
                .then(() => {
                    AccountActions.tryToSetCurrentAccount();
                    this.getUserId(this.props.currentAccount);
                })
                .catch(() => {});
        } else {
            WalletUnlockActions.lock();
            if (!WalletUnlockStore.getState().rememberMe) {
                if (!isPersistantType()) {
                    setLocalStorageType("persistant");
                }
                AccountActions.setPasswordAccount(null);
                AccountStore.tryToSetCurrentAccount();
            }
        }
        this._closeDropdown();
    }

    _onNavigate(route, e) {
        e.preventDefault();

        // Set Accounts Tab as active tab
        if (route == "/accounts") {
            SettingsActions.changeViewSetting({
                dashboardEntry: "accounts"
            });
        }

        this.props.history.push(route);
        this._closeDropdown();
    }

    _closeDropdown() {
        this.setState({
            dropdownActive: false,
            mobileMenuActive: false
        });
    }

    _onGoBack(e) {
        e.preventDefault();
        window.history.back();
    }

    _onGoForward(e) {
        e.preventDefault();
        window.history.forward();
    }

    _toggleDropdownMenu() {
        this.setState({
            dropdownActive: !this.state.dropdownActive
        });
    }

    onBodyClick() {
        this.setState({
            dropdownActive: false
        });
    }

    toggleMobileMenuOn = () => {
        let mobileMenuStatus = this.state.mobileMenuActive;

        if (mobileMenuStatus) {
            this.setState({
                mobileMenuActive: false
            });
        } else {
            this.setState({
                mobileMenuActive: true
            });
        }
    };

    toggleMobileMenuOff = () => {
        this.setState({
            mobileMenuActive: false
        });
    };

    render() {
        let {active, mobileMenuActive} = this.state;
        let {
            currentAccount,
            starredAccounts,
            passwordLogin,
            passwordAccount
        } = this.props;

        // console.log("active", active);

        let isAmbassador;
        let ambassadorsList = ChainStore.fetchFullAccount("ambassadors-list");

        if (this.props.account && ambassadorsList) {
            isAmbassador =
                ambassadorsList
                    .get("whitelisted_accounts")
                    .indexOf(this.props.account.get("id")) >= 0;
        }

        /* Dynamic Menu Item */
        let dynamicMenuItem;

        if (active.indexOf("settings") !== -1) {
            dynamicMenuItem = (
                <a
                    style={{flexFlow: "row"}}
                    className={cnames({
                        active: active.indexOf("settings") !== -1
                    })}
                >
                    <Icon
                        size="1_5x"
                        style={{position: "relative", top: 0, left: -8}}
                        name="settings"
                        title="icons.cogs"
                    />
                    <Translate
                        className="column-hide-small"
                        component="span"
                        content="header.settings"
                    />
                </a>
            );
        }
        if (active.indexOf("/help") !== -1) {
            dynamicMenuItem = (
                <a
                    style={{flexFlow: "row"}}
                    className={cnames({active: active.indexOf("help") !== -1})}
                >
                    <Icon
                        size="1_5x"
                        style={{position: "relative", top: 0, left: -8}}
                        name="crowdwiki"
                        title="icons.crowdwiki"
                    />
                    <span>CrowdWIKI</span>
                </a>
            );
        }
        if (active.indexOf("/voting") !== -1) {
            dynamicMenuItem = (
                <a style={{flexFlow: "row"}}>
                    <Icon
                        size="1_5x"
                        style={{position: "relative", top: 0, left: -8}}
                        name="thumbs-up"
                        title="icons.thumbs_up"
                    />
                    <Translate
                        className="column-hide-small"
                        component="span"
                        content="account.voting"
                    />
                </a>
            );
        }
        if (
            active.indexOf("/assets") !== -1 &&
            active.indexOf("explorer") === -1
        ) {
            dynamicMenuItem = (
                <a
                    style={{flexFlow: "row"}}
                    className={cnames({
                        active: active.indexOf("/assets") !== -1
                    })}
                >
                    <Icon
                        size="1_5x"
                        style={{position: "relative", top: 0, left: -8}}
                        name="finance"
                        title="icons.assets"
                    />
                    <Translate
                        className="column-hide-small"
                        component="span"
                        content="explorer.assets.title"
                    />
                </a>
            );
        }
        if (active.indexOf("/signedmessages") !== -1) {
            dynamicMenuItem = (
                <a
                    style={{flexFlow: "row"}}
                    className={cnames({
                        active: active.indexOf("/signedmessages") !== -1
                    })}
                >
                    <Icon
                        size="1_5x"
                        style={{position: "relative", top: 0, left: -8}}
                        name="mail"
                        title="icons.text.signed_messages"
                    />
                    <Translate
                        className="column-hide-small"
                        component="span"
                        content="account.signedmessages.menuitem"
                    />
                </a>
            );
        }
        if (active.indexOf("/vesting") !== -1) {
            dynamicMenuItem = (
                <a style={{flexFlow: "row"}}>
                    <Icon
                        size="1_5x"
                        style={{position: "relative", top: 0, left: -8}}
                        name="vesting-desc"
                        title="header.my_status"
                    />
                    <Translate
                        className="column-hide-small"
                        component="span"
                        content="account.vesting.status_header"
                    />
                </a>
            );
        }
        if (active.indexOf("/structure") !== -1) {
            dynamicMenuItem = (
                <a style={{flexFlow: "row"}}>
                    <Icon
                        size="1_5x"
                        style={{position: "relative", top: 0, left: -8}}
                        name="activity"
                        title="header.structure"
                    />
                    <Translate
                        className="column-hide-small"
                        component="span"
                        content="account.structure.structure_header"
                    />
                </a>
            );
        }
        if (active.indexOf("/gamezone") !== -1) {
            dynamicMenuItem = (
                <a style={{flexFlow: "row"}}>
                    <Icon
                        size="1_5x"
                        style={{position: "relative", top: 0, left: -8}}
                        name="gamepad"
                        title="icons.gamezone"
                    />
                    <Translate
                        className="column-hide-small"
                        component="span"
                        content="account.gamezone.gamezone_header"
                    />
                </a>
            );
        }
        if (active.indexOf("/whitelist") !== -1) {
            dynamicMenuItem = (
                <a
                    style={{flexFlow: "row"}}
                    className={cnames({
                        active: active.indexOf("/whitelist") !== -1
                    })}
                >
                    <Icon
                        size="1_5x"
                        style={{position: "relative", top: 0, left: -8}}
                        name="white_list"
                        title="icons.list"
                    />
                    <Translate
                        className="column-hide-small"
                        component="span"
                        content="account.whitelist.title"
                    />
                </a>
            );
        }
        if (active.indexOf("/permissions") !== -1) {
            dynamicMenuItem = (
                <a
                    style={{flexFlow: "row"}}
                    className={cnames({
                        active: active.indexOf("/permissions") !== -1
                    })}
                >
                    <Icon
                        size="1_5x"
                        style={{position: "relative", top: 0, left: -8}}
                        name="key2"
                        title="icons.warning"
                    />
                    <Translate
                        className="column-hide-small"
                        component="span"
                        content="account.permissions"
                    />
                </a>
            );
        }
        if (active.indexOf("/blocks") !== -1) {
            dynamicMenuItem = (
                <a
                    style={{flexFlow: "row"}}
                    className={cnames({
                        active: active.indexOf("/permissions") !== -1
                    })}
                >
                    <Icon
                        size="1_5x"
                        style={{position: "relative", top: 0, left: -8}}
                        name="eye"
                        title="account.explorer"
                    />
                    <Translate
                        className="column-hide-small"
                        component="span"
                        content="account.explorer"
                    />
                </a>
            );
        }

        if (active.indexOf("account") !== -1) {
            var str = "/account/";
            var url = active.substring(str.length, active.length);

            if (url.search(/\//) == -1) {
                dynamicMenuItem = (
                    <a
                        style={{flexFlow: "row"}}
                        className={cnames({
                            active: active.indexOf("/account") !== -1
                        })}
                    >
                        <Icon
                            size="1_5x"
                            style={{position: "relative", top: 0, left: -8}}
                            name="arrow_4"
                            title="account.activity"
                        />
                        <Translate
                            className="column-hide-small"
                            component="span"
                            content="account.activity"
                        />
                    </a>
                );
            }
        }

        if (active.indexOf("/crowdmarket") !== -1) {
            dynamicMenuItem = (
                <a
                    style={{flexFlow: "row"}}
                    className={cnames({
                        active: active.indexOf("/crowdmarket") !== -1
                    })}
                >
                    <Icon
                        size="1_5x"
                        style={{position: "relative", top: 0, left: -8}}
                        name="crowdmarket-icon"
                        title="crowdmarket.crowdmarket_header"
                    />
                    <Translate
                        className="column-hide-small"
                        component="span"
                        content="crowdmarket.crowdmarket_header"
                    />
                </a>
            );
        }
        if (
            active.indexOf("/market/GCWD_CWD") !== -1 ||
            active.indexOf("/market/CROWD.BTC_CWD") !== -1
        ) {
            dynamicMenuItem = (
                <a
                    style={{flexFlow: "row"}}
                    className={cnames({
                        active:
                            active.indexOf("/market/GCWD_CWD") !== -1 ||
                            active.indexOf("/market/CROWD.BTC_CWD") !== -1
                    })}
                >
                    <Icon
                        size="1_5x"
                        style={{position: "relative", top: 0, left: -8}}
                        name="exchange"
                        title="header.exchange"
                    />
                    <Translate
                        className="column-hide-small"
                        component="span"
                        content="header.exchange"
                    />
                </a>
            );
        }
        if (active.indexOf("/gateway") !== -1) {
            dynamicMenuItem = (
                <a
                    style={{flexFlow: "row"}}
                    className={cnames({
                        active: active.indexOf("/gateway") !== -1
                    })}
                >
                    <Icon
                        size="1_5x"
                        style={{position: "relative", top: 0, left: -8}}
                        name="exchange2"
                        title="crowdmarket.crowdmarket_header"
                    />
                    <Translate
                        className="column-hide-small"
                        component="span"
                        content="cwdgateway.exchange.tab"
                    />
                </a>
            );
        }

        let tradingAccounts = AccountStore.getMyAccounts();

        const a = ChainStore.getAccount(currentAccount);
        const showAccountLinks = !!a;

        if (starredAccounts.size) {
            for (let i = tradingAccounts.length - 1; i >= 0; i--) {
                if (!starredAccounts.has(tradingAccounts[i])) {
                    tradingAccounts.splice(i, 1);
                }
            }
            starredAccounts.forEach(account => {
                if (tradingAccounts.indexOf(account.name) === -1) {
                    tradingAccounts.push(account.name);
                }
            });
        }

        let walletBalance = this.props.currentAccount ? (
            <div className="total-value">
                <TotalBalanceValue.AccountWrapper
                    hiddenAssets={this.props.hiddenAssets}
                    accounts={List([this.props.currentAccount])}
                    noTip
                    style={{minHeight: 15}}
                />
            </div>
        ) : null;

        this.getUserId(this.props.currentAccount);

        let contract_icon;

        if (this.state.status == 0) {
            contract_icon = <img className="header__icon" src={client} />;
        }
        if (this.state.status == 1) {
            contract_icon = <img className="header__icon" src={start} />;
        }
        if (this.state.status == 2) {
            contract_icon = <img className="header__icon" src={expert} />;
        }
        if (this.state.status == 3) {
            contract_icon = <img className="header__icon" src={citizen} />;
        }
        if (this.state.status == 4) {
            contract_icon = <img className="header__icon" src={infinity} />;
        }

        let width = window.innerWidth;

        return (
            <div className="header__container">
                {width > 576 ? (
                    <div className="header menu-group primary">
                        {/* {__ELECTRON__ ? (
                            <div className="grid-block show-for-medium shrink electron-navigation">
                                <ul className="menu-bar">
                                    <li>
                                        <div
                                            style={{
                                                marginLeft: "1rem",
                                                height: "3rem"
                                            }}
                                        >
                                            <div
                                                style={{ marginTop: "0.5rem" }}
                                                onClick={this._onGoBack.bind(
                                                    this
                                                )}
                                                className="button outline small"
                                            >
                                                {"<"}
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div
                                            style={{
                                                height: "3rem",
                                                marginLeft: "0.5rem",
                                                marginRight: "0.75rem"
                                            }}
                                        >
                                            <div
                                                style={{ marginTop: "0.5rem" }}
                                                onClick={this._onGoForward.bind(
                                                    this
                                                )}
                                                className="button outline small"
                                            >
                                                >
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        ) : null} */}

                        <ul className="menu-bar">
                            {/* MAIN PAGE LINK */}
                            <li>
                                <Link to={`/`}>
                                    <img className="header__logo" src={logo} />
                                </Link>
                            </li>
                            {/* DROPDOWN MENU */}
                            <li className="header__current-component">
                                {dynamicMenuItem}
                            </li>
                        </ul>
                    </div>
                ) : null}

                {/* ACCOUNT BLOCK */}
                {this.props.currentAccount ? (
                    <div className="header__account-block">
                        <div className="truncated active-account">
                            <div className="header__account-inner">
                                {/* CONTRACT ICON */}
                                <div className="header__contract-wrap">
                                    {contract_icon}
                                </div>

                                <div className="header__balance-wrap">
                                    {/* NAME BLOCK */}
                                    <div
                                        className="header__name-wrap text account-name"
                                        onClick={this._toggleDropdownMenu}
                                    >
                                        {isAmbassador ? (
                                            <Icon
                                                className="header__ambassador-icon"
                                                name="ambassador-icon"
                                                title="icons.ambassador"
                                            />
                                        ) : null}
                                        <span className="header__name">
                                            {currentAccount}
                                        </span>
                                        <div className="header__drop-icon-wrap">
                                            <span className="header__drop-icon"></span>
                                        </div>
                                    </div>

                                    {/* BALANCE BLOCK */}
                                    {walletBalance}
                                </div>

                                {/* DROPDOWN MENU */}
                                {this.state.dropdownActive ? (
                                    <div className="header__menu-block">
                                        <DropDownMenu
                                            toggleLock={this._toggleLock.bind(
                                                this
                                            )}
                                            locked={this.props.locked}
                                            active={active}
                                            passwordLogin={passwordLogin}
                                            onNavigate={this._onNavigate.bind(
                                                this
                                            )}
                                            showAccountLinks={showAccountLinks}
                                            currentAccount={currentAccount}
                                            account={this.props.account}
                                        />
                                    </div>
                                ) : null}
                            </div>
                        </div>

                        <div className="header__btn-wrap">
                            {/* MOBILE VIEW */}
                            {width < 576 ? (
                                <div className="header__connection-wrap">
                                    {/* COONECTION INDICATOR */}
                                    <Footer
                                        synced={this.state.synced}
                                        history={this.props.history}
                                        toggleFooter={false}
                                    />
                                    {/* MOBILE MENU BTN*/}
                                    <div className="header__hamburger-mobile">
                                        <Icon
                                            className="icon"
                                            name="hamburger"
                                            title="icons.hamburger"
                                            onClick={this.toggleMobileMenuOn}
                                        />
                                    </div>
                                </div>
                            ) : null}

                            {/* LOCK / UNLOCK */}
                            <div className="header__unlock-wrap locked">
                                <Icon
                                    className="lock-unlock"
                                    size="2x"
                                    name={
                                        this.props.locked
                                            ? "locked-icon"
                                            : "unlocked-icon"
                                    }
                                    title={
                                        this.props.locked
                                            ? "icons.locked.common"
                                            : "icons.unlocked.common"
                                    }
                                    onClick={this._toggleLock.bind(this)}
                                />
                            </div>
                        </div>

                        {/* MOBILE MENU */}
                        {mobileMenuActive ? (
                            <MobileMenu
                                currentAccount={currentAccount}
                                history={this.props.history}
                                onNavigate={this._onNavigate.bind(this)}
                                locked={this.props.locked}
                                closeDropdown={this._closeDropdown}
                            />
                        ) : null}
                    </div>
                ) : (
                    // NON-RESISTRATION VIEW
                    <div className="header__account-block">
                        <div className="header__register-block">
                            {/* REGISTTRATION BTN */}
                            <div
                                className="header__register-inner"
                                onClick={this._onNavigate.bind(
                                    this,
                                    "/create-account/password"
                                )}
                            >
                                <Icon
                                    size="1_5x"
                                    name="add_account"
                                    title="icons.user.create_account"
                                />
                                <div className="header__register-text">
                                    <Translate content="account.nav_menu.register" />
                                </div>
                            </div>

                            {/* MOBILE MENU BTN*/}
                            {width < 576 ? (
                                <div className="header__hamburger-mobile">
                                    <Icon
                                        className="icon-30px"
                                        name="hamburger"
                                        title="icons.hamburger"
                                        onClick={this.toggleMobileMenuOn}
                                    />
                                </div>
                            ) : null}

                            {/* MOBILE MENU */}
                            {mobileMenuActive ? (
                                <MobileMenu
                                    currentAccount={currentAccount}
                                    history={this.props.history}
                                    onNavigate={this._onNavigate.bind(this)}
                                    closeDropdown={this._closeDropdown}
                                    locked={this.props.locked}
                                />
                            ) : null}
                        </div>
                    </div>
                )}
            </div>
        );
    }
}

Header = connect(Header, {
    listenTo() {
        return [
            AccountStore,
            WalletUnlockStore,
            WalletManagerStore,
            SettingsStore
        ];
    },
    getProps() {
        if (AccountStore.getState().currentAccount) {
            return {
                currentAccount:
                    AccountStore.getState().currentAccount ||
                    AccountStore.getState().passwordAccount,
                passwordAccount: AccountStore.getState().passwordAccount,
                locked: WalletUnlockStore.getState().locked,
                account: ChainStore.fetchFullAccount(
                    AccountStore.getState().currentAccount
                ),

                starredAccounts: AccountStore.getState().starredAccounts,
                passwordLogin: SettingsStore.getState().settings.get(
                    "passwordLogin"
                ),
                currentLocale: SettingsStore.getState().settings.get("locale"),
                hiddenAssets: SettingsStore.getState().hiddenAssets,
                settings: SettingsStore.getState().settings,
                locales: SettingsStore.getState().defaults.locale
            };
        } else {
            return {
                currentAccount:
                    AccountStore.getState().currentAccount ||
                    AccountStore.getState().passwordAccount,
                passwordAccount: AccountStore.getState().passwordAccount,
                locked: WalletUnlockStore.getState().locked,

                starredAccounts: AccountStore.getState().starredAccounts,
                passwordLogin: SettingsStore.getState().settings.get(
                    "passwordLogin"
                ),
                currentLocale: SettingsStore.getState().settings.get("locale"),
                hiddenAssets: SettingsStore.getState().hiddenAssets,
                settings: SettingsStore.getState().settings,
                locales: SettingsStore.getState().defaults.locale
            };
        }
    }
});

export default withRouter(Header);
