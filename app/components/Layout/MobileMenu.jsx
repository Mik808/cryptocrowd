import React from "react";
import SendModal from "../Modal/SendModal";
import Icon from "../Icon/Icon";
import Translate from "react-translate-component";
import {ChainStore} from "bitsharesjs";
import Footer from "./Footer";

class MobileMenu extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isDepositModalVisible: false,
            hasDepositModalBeenShown: false,
            isWithdrawModalVisible: false,
            hasWithdrawalModalBeenShown: false,
            synced: this._syncStatus(),
            finance: false,
            exchange: false,
            active: this.props.history,
            locked: this.props.locked
        };

        this._syncStatus = this._syncStatus.bind(this);
    }

    componentDidMount() {
        this.syncCheckInterval = setInterval(
            this._syncStatus.bind(this, true),
            5000
        );
    }

    componentWillMount() {
        this.unlisten = this.props.history.listen(newState => {
            if (this.unlisten && this.state.active !== newState.pathname) {
                this.setState({
                    active: newState.pathname
                });
            }
        });
    }

    UNSAFE_componentWillUnmount() {
        clearInterval(this.syncCheckInterval);
    }

    toggleFinance = () => {
        this.setState({
            finance: !this.state.finance,
            exchange: false
        });
    };

    toggleExchangeOn = () => {
        this.setState({
            exchange: !this.state.exchange,
            finance: false
        });
    };

    _showSend = e => {
        e.preventDefault();
        if (this.send_modal) {
            this.send_modal.show();
        }
    };

    _syncStatus(setState = false) {
        let synced = this.getBlockTimeDelta() < 5;
        if (setState && synced !== this.state.synced) {
            this.setState({synced});
        }
        return synced;
    }

    getBlockTimeDelta() {
        try {
            let bt =
                (this.getBlockTime().getTime() +
                    ChainStore.getEstimatedChainTimeOffset()) /
                1000;
            let now = new Date().getTime() / 1000;
            return Math.abs(now - bt);
        } catch (err) {
            return -1;
        }
    }

    render() {
        let currentAccount = this.props.currentAccount;
        let {finance, exchange} = this.state;

        {
            /*FINANCE MENU BLOCK*/
        }

        const financeSubMenu = finance
            ? [
                  {
                      mainText: "header.payments",
                      mainCallback: this._showSend,
                      subText: "header.payments_legacy",
                      subURL: "/transfer"
                  },
                  {
                      mainText: "header.deposit_withdraw",
                      mainCallback: this.props.onNavigate.bind(this, "/gateway")
                  },
                  {
                      mainText: "header.invest",
                      mainCallback: this.props.onNavigate.bind(
                          this,
                          "/investment"
                      )
                  },
                  {
                      mainText: "account.portfolio_menu",
                      mainCallback: this.props.onNavigate.bind(
                          this,
                          `/account/${this.props.currentAccount}/portfolio`
                      )
                  }
              ].map(({mainText, mainCallback}, index) => (
                  <li onClick={mainCallback} key={index}>
                      <div>
                          <Translate content={mainText} />
                      </div>
                  </li>
              ))
            : null;

        const financeMenuToggleOn = !this.state.locked ? (
            <li className="leftmenu__li--mobile">
                <div>
                    <Icon size="1_5x" name="finance" title="icons.finance" />
                </div>
                <div onClick={this.toggleFinance}>
                    <Translate content="header.finance" />
                </div>
                <ul className="leftmenu__submenu">{financeSubMenu}</ul>
            </li>
        ) : null;

        return (
            <div className="header__mobile-menu">
                <div className="leftmenu__container--mobile">
                    <ul className="leftmenu__ul--mobile">
                        <li
                            className="leftmenu__li--mobile"
                            onClick={this.props.onNavigate.bind(
                                this,
                                "/gamezone"
                            )}
                        >
                            <div>
                                <Icon
                                    size="1_5x"
                                    name="gamepad"
                                    title="account.gamezone.gamezone_header"
                                />
                            </div>
                            <div>
                                <Translate content="account.gamezone.gamezone_header" />
                            </div>
                        </li>

                        <li
                            className="leftmenu__li--mobile"
                            onClick={this.props.onNavigate.bind(
                                this,
                                "/crowdmarket"
                            )}
                        >
                            <div>
                                <Icon
                                    size="1_5x"
                                    name="crowdmarket-icon"
                                    title="crowdmarket.crowdmarket_header"
                                />
                            </div>
                            <div>
                                <Translate content="crowdmarket.crowdmarket_header" />
                            </div>
                        </li>

                        {this.props.currentAccount ? financeMenuToggleOn : null}

                        <li
                            className="leftmenu__li--mobile"
                            onClick={this.props.onNavigate.bind(
                                this,
                                "/markets-dashboard"
                            )}
                        >
                            <div>
                                <Icon
                                    size="1_5x"
                                    name="exchange"
                                    title="header.exchange"
                                />
                            </div>
                            <div>
                                <Translate content="header.exchange" />
                            </div>
                        </li>

                        {this.props.currentAccount ? (
                            <li
                                className="leftmenu__li--mobile"
                                onClick={this.props.onNavigate.bind(
                                    this,
                                    `/account/${this.props.currentAccount}/vesting`
                                )}
                            >
                                <div>
                                    <Icon
                                        size="1_5x"
                                        name="vesting-desc"
                                        title="header.my_status"
                                    />
                                </div>
                                <div>
                                    <Translate content="header.my_status" />
                                </div>
                            </li>
                        ) : null}

                        {this.props.currentAccount ? (
                            <li
                                className="leftmenu__li--mobile"
                                onClick={this.props.onNavigate.bind(
                                    this,
                                    `/account/${this.props.currentAccount}/structure`
                                )}
                            >
                                <div>
                                    <Icon
                                        size="1_5x"
                                        name="activity"
                                        title="header.structure"
                                    />
                                </div>
                                <div>
                                    <Translate content="header.structure" />
                                </div>
                            </li>
                        ) : null}

                        {this.props.currentAccount ? (
                            <li
                                className="leftmenu__li--mobile"
                                onClick={this.props.onNavigate.bind(
                                    this,
                                    `/account/${this.props.currentAccount}/voting`
                                )}
                            >
                                <div>
                                    <Icon
                                        size="1_5x"
                                        name="vote"
                                        title="account.voting"
                                    />
                                </div>
                                <div>
                                    <Translate content="account.voting" />
                                </div>
                            </li>
                        ) : null}

                        {this.props.currentAccount ? (
                            <li
                                className="leftmenu__li--mobile"
                                onClick={this.props.onNavigate.bind(
                                    this,
                                    `/account/${this.props.currentAccount}`
                                )}
                            >
                                <div>
                                    <Icon
                                        size="1_5x"
                                        name="arrow_4"
                                        title="account.activity"
                                    />
                                </div>
                                <div>
                                    <Translate content="account.activity" />
                                </div>
                            </li>
                        ) : null}

                        <li
                            className="leftmenu__li--mobile"
                            onClick={this.props.onNavigate.bind(
                                this,
                                "/explorer/blocks"
                            )}
                        >
                            <div>
                                <Icon
                                    size="1_5x"
                                    name="eye"
                                    title="account.explorer"
                                />
                            </div>
                            <div>
                                <Translate content="account.explorer" />
                            </div>
                        </li>

                        <li
                            className="leftmenu__li--mobile"
                            onClick={this.props.onNavigate.bind(this, "/")}
                        >
                            <div>
                                <Icon
                                    size="1_5x"
                                    name="logo-light"
                                    title="account.nav_menu.main_page"
                                />
                            </div>
                            <div>
                                <Translate content="account.nav_menu.main_page" />
                            </div>
                        </li>
                    </ul>

                    <SendModal
                        id="send_modal_header"
                        refCallback={e => {
                            if (e) this.send_modal = e;
                        }}
                        from_name={currentAccount}
                    />
                </div>

                <div className="leftmenu__footer-wrap">
                    <Footer
                        synced={this.state.synced}
                        history={this.props.history}
                        toggleFooter={true}
                        closeDropdown={this.props.closeDropdown}
                    />
                </div>
            </div>
        );
    }
}

export default MobileMenu;
