import React from "react";
import Icon from "../Icon/Icon";
import Translate from "react-translate-component";
import cnames from "classnames";
import AccountActions from "actions/AccountActions";
import {ChainStore} from "bitsharesjs";

export default class DropDownMenu extends React.Component {
    shouldComponentUpdate(np) {
        let shouldUpdate = false;
        for (let key in np) {
            if (typeof np[key] === "function") continue;
            shouldUpdate = shouldUpdate || np[key] !== this.props[key];
        }
        return shouldUpdate;
    }

    _onAddContact() {
        AccountActions.addAccountContact(this.props.currentAccount);
    }

    _onRemoveContact() {
        AccountActions.removeAccountContact(this.props.currentAccount);
    }

    render() {
        const {active, showAccountLinks, currentAccount} = this.props;

        let account = this.props.account.toJS();
        let membership_expiration_date = account.membership_expiration_date;

        if (
            membership_expiration_date === "1969-12-31T23:59:59" ||
            membership_expiration_date === "1970-01-01T00:00:00"
        )
            membership_expiration_date = false;
        else {
            membership_expiration_date = true;
        }

        return (
            <ul className="header__dropdown-ul dropdown header-menu">
                {/* {currentAccount ? (
                    <li
                        className={cnames({
                            active: active.indexOf("/signedmessages") !== -1,
                            disabled: !showAccountLinks
                        })}
                        onClick={this.props.onNavigate.bind(
                            this,
                            `/account/${currentAccount}/signedmessages`
                        )}
                    >
                        <div className="table-cell">
                            <Icon
                                size="1_5x"
                                name="mail"
                                title="icons.text.signed_messages"
                            />
                        </div>
                        <div className="table-cell">
                            <Translate content="account.signedmessages.menuitem" />
                        </div>
                    </li>
                ) : null} */}

                {currentAccount ? (
                    <li
                        className={cnames({
                            active: active.indexOf("/whitelist") !== -1,
                            disabled: !showAccountLinks
                        })}
                        onClick={this.props.onNavigate.bind(
                            this,
                            `/account/${currentAccount}/whitelist`
                        )}
                    >
                        <div className="table-cell">
                            <Icon
                                size="1_5x"
                                name="white_list"
                                title="icons.list"
                            />
                        </div>
                        <div className="table-cell">
                            <Translate content="account.whitelist.title" />
                        </div>
                    </li>
                ) : null}

                {currentAccount ? (
                    <li
                        className={cnames("divider", {
                            active: active.indexOf("/permissions") !== -1,
                            disabled: !showAccountLinks
                        })}
                        onClick={this.props.onNavigate.bind(
                            this,
                            `/account/${currentAccount}/permissions`
                        )}
                    >
                        <div className="table-cell">
                            <Icon
                                size="1_5x"
                                name="key2"
                                title="icons.warning"
                            />
                        </div>
                        <div className="table-cell">
                            <Translate content="account.permissions" />
                        </div>
                    </li>
                ) : null}

                {membership_expiration_date ? (
                    <li
                        className={cnames("divider", {
                            active: active.indexOf("/assets") !== -1,
                            disabled: !showAccountLinks
                        })}
                        onClick={this.props.onNavigate.bind(
                            this,
                            `/account/${currentAccount}/assets`
                        )}
                    >
                        <div className="table-cell">
                            <Icon
                                size="1_5x"
                                name="finance"
                                title="icons.assets"
                            />
                        </div>
                        <div className="table-cell">
                            <Translate content="explorer.assets.title" />
                        </div>
                    </li>
                ) : null}

                <li
                    className={cnames(
                        {
                            active: active.indexOf("/settings") !== -1
                        },
                        "divider",
                        "desktop-only"
                    )}
                    onClick={this.props.onNavigate.bind(this, "/settings")}
                >
                    <div className="table-cell">
                        <Icon size="1_5x" name="settings" title="icons.cogs" />
                    </div>
                    <div className="table-cell">
                        <Translate content="header.settings" />
                    </div>
                </li>
            </ul>
        );
    }
}
