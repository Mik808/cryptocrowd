import React from "react";
import {connect} from "alt-react";
import counterpart from "counterpart";
import AccountStore from "stores/AccountStore";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";
import TranslateWithLinks from "./Utility/TranslateWithLinks";
import {isIncognito} from "feature_detect";
import SettingsActions from "actions/SettingsActions";
import WalletUnlockActions from "actions/WalletUnlockActions";
import SettingsStore from "stores/SettingsStore";
import IntlActions from "actions/IntlActions";
import CreateAccountPassword from "./Account/CreateAccountPassword";
import {Route} from "react-router-dom";
import {getWalletName, getRegLogo} from "branding";
import {Select, Row, Col} from "crowdwiz-ui-modal";
import WalletDb from "stores/WalletDb";
import AccountActions from "actions/AccountActions";
import WalletUnlockStore from "stores/WalletUnlockStore";
import Icon from "./Icon/Icon";
import {setLocalStorageType, isPersistantType} from "lib/common/localStorage";

var logo = getRegLogo();

const FlagImage = ({flag, width = 50, height = 50}) => {
    return (
        <img
            height={height}
            width={width}
            src={`${__BASE_URL__}language-dropdown/${flag.toUpperCase()}.png`}
        />
    );
};

class LoginSelector extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            step: 1,
            locales: SettingsStore.getState().defaults.locale,
            currentLocale: SettingsStore.getState().settings.get("locale")
        };
        this.unmounted = false;

        this.handleLanguageSelect = this.handleLanguageSelect.bind(this);
    }

    UNSAFE_componentWillMount() {
        isIncognito(incognito => {
            if (!this.unmounted) {
                this.setState({incognito});
            }
        });
    }

    componentWillUnmount() {
        this.unmounted = true;
    }

    onSelect(route) {
        this.props.history.push("/create-account/" + route);
    }

    handleLanguageSelect(locale) {
        IntlActions.switchLocale(locale);
        this.setState({
            currentLocale: locale
        });
    }

    languagesFilter(input, option) {
        return (
            option.props.language.toLowerCase().indexOf(input.toLowerCase()) >=
            0
        );
    }

    _toggleLock(e) {
        e.preventDefault();
        if (WalletDb.isLocked()) {
            WalletUnlockActions.unlock()
                .then(() => {
                    AccountActions.tryToSetCurrentAccount();
                })
                .catch(() => {});
        } else {
            WalletUnlockActions.lock();
            if (!WalletUnlockStore.getState().rememberMe) {
                if (!isPersistantType()) {
                    setLocalStorageType("persistant");
                }
                AccountActions.setPasswordAccount(null);
                AccountStore.tryToSetCurrentAccount();
            }
        }
        // this._closeDropdown();
    }

    render() {
        const translator = require("counterpart");
        let accountName = AccountStore.getState().currentAccount;

        const flagDropdown = (
            <Select
                showSearch
                filterOption={this.languagesFilter}
                value={this.state.currentLocale}
                onChange={this.handleLanguageSelect}
                style={{width: "123px", marginBottom: "16px"}}
            >
                {this.state.locales.map(locale => (
                    <Select.Option
                        key={locale}
                        language={counterpart.translate("languages." + locale)}
                    >
                        {counterpart.translate("languages." + locale)}
                    </Select.Option>
                ))}
            </Select>
        );

        return (
            <div className="grid-block align-center" id="accountForm">
                <div className="grid-block shrink vertical">
                    <div className="grid-content shrink text-center account-creation">
                        <div>
                            <img src={logo} />
                        </div>

                        <Translate
                            content="header.create_account"
                            component="h4"
                            className="registration-header"
                        />

                        <Route
                            path="/create-account/password"
                            exact
                            component={CreateAccountPassword}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(LoginSelector, {
    listenTo() {
        return [AccountStore];
    },
    getProps() {
        return {
            currentAccount:
                AccountStore.getState().currentAccount ||
                AccountStore.getState().passwordAccount
        };
    }
});
