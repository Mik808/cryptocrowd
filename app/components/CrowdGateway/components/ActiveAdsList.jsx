import React from "react";
import {Tabs, Tab} from "../../Utility/Tabs";
import TradeItemBuy from "./TradeItemBuy";
import TradeItemSell from "./TradeItemSell";
import {Apis} from "bitsharesjs-ws";
import Translate from "react-translate-component";

//STYLES
import "../scss/cwdgateway-active.scss";
import "../scss/cwdgateway-popup.scss";

class ActiveAdsList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            buyAds: [],
            sellAds: [],
            currentAccount: this.props.currentAccount,
            valideAmount: false,
            intervalID: 0
        };
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (
            JSON.stringify(nextState.buyAds) !==
                JSON.stringify(this.state.buyAds) ||
            JSON.stringify(nextState.sellAds) !==
                JSON.stringify(this.state.sellAds)
        );
    }

    componentDidMount() {
        this.getAds();

        this.setState({
            intervalID: setInterval(this.getAds.bind(this), 5000)
        });
    }

    componentWillUnmount() {
        clearInterval(this.state.intervalID);
    }

    getAds() {
        let currentAccount = this.props.currentAccount;

        Apis.instance()
            .db_api()
            .exec("get_p2p_adv", [currentAccount, 1])
            .then(tradesBuy => {
                this.setState({
                    buyAds: tradesBuy
                });
            });
        Apis.instance()
            .db_api()
            .exec("get_p2p_adv", [currentAccount, 0])
            .then(tradesSell => {
                this.setState({
                    sellAds: tradesSell
                });
            });
    }

    render() {
        let buyAds = this.state.buyAds;
        let sellAds = this.state.sellAds;
        let currentAccount = this.state.currentAccount;

        return (
            <div>
                <Tabs
                    className="cwd-tabs cwd-tabs--gateway-mode"
                    tabsClass="cwd-tabs__list"
                    contentClass="cwd-tabs__content"
                    segmented={false}
                    actionButtons={false}
                >
                    <Tab title="cwdgateway.ads.buy_cwd">
                        {buyAds.length > 0 ? (
                            <ul className="cwdgateway-active__list">
                                {buyAds
                                    .sort((a, b) => {
                                        if (a.rating > b.rating) return -1;
                                        else if (a.rating < b.rating) return 1;
                                        return 0;
                                    })
                                    .map(activeItem =>
                                        activeItem["pa"]["max_cwd"] >=
                                        activeItem["pa"]["min_cwd"] ? (
                                            <TradeItemBuy
                                                key={activeItem["pa"]["id"]}
                                                buyAds={activeItem}
                                                currentAccount={currentAccount}
                                            />
                                        ) : null
                                    )}
                            </ul>
                        ) : (
                            <Translate
                                className="cwdgateway__no-data"
                                content="cwdgateway.ads.no_buy_trades"
                            />
                        )}
                    </Tab>

                    <Tab title="cwdgateway.ads.sell_cwd">
                        {sellAds.length > 0 ? (
                            <ul className="cwdgateway-active__list">
                                {sellAds
                                    .sort((a, b) => {
                                        if (a.rating > b.rating) return -1;
                                        else if (a.rating < b.rating) return 1;
                                        return 0;
                                    })
                                    .map(activeItem => (
                                        <TradeItemSell
                                            key={activeItem["pa"]["id"]}
                                            sellAds={activeItem}
                                            currentAccount={currentAccount}
                                        />
                                    ))}
                            </ul>
                        ) : (
                            <Translate
                                className="cwdgateway__no-data"
                                content="cwdgateway.ads.no_sell_trades"
                            />
                        )}
                    </Tab>
                </Tabs>
            </div>
        );
    }
}

export default ActiveAdsList;
