import React from "react";
import Translate from "react-translate-component";
import Icon from "../../Icon/Icon";
import {Link} from "react-router-dom";
import TransferBTC from "./TransferBTC";
import {connect} from "alt-react";
import AccountStore from "stores/AccountStore";
import {ChainStore} from "bitsharesjs";
import {Apis} from "bitsharesjs-ws";

//STYLES
import "../scss/cwdgateway.scss";
import "../scss/gateways/gatewayBTC.scss";
import "../scss/gateways/gatewayTransfer.scss";

class GatewayBTC extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentAccount: this.props.account,
            userBalance: 0,
            addressBTC: "",
            showClipboardMsg: false
        };

        this.getWalletAddress = this.getWalletAddress.bind(this);
        this.getBalance = this.getBalance.bind(this);
        this.copyValue = this.copyValue.bind(this);
    }

    componentDidMount() {
        if (this.props.account) {
            let currentAccount = this.state.currentAccount;
            let userID = currentAccount.get("id");
            let userName = currentAccount.get("name");

            this.getWalletAddress(userName);
            this.getBalance(userID, "CROWD.BTC");
        } else {
            this.props.history.push("/create-account/password");
        }
    }

    getBalance(user, asset) {
        let precision;
        let assetID;

        Apis.instance()
            .db_api()
            .exec("get_assets", [[asset]])
            .then(assetObj => {
                precision = assetObj[0]["precision"];
                assetID = assetObj[0]["id"];

                Apis.instance()
                    .db_api()
                    .exec("get_account_balances", [user, [assetID]])
                    .then(accountObj => {
                        this.setState({
                            userBalance:
                                accountObj[0]["amount"] /
                                Math.pow(10, precision)
                        });
                    });
            });
    }

    getWalletAddress(name) {
        var url = "https://api.crowdwiz.biz/deposit?account=" + name;

        fetch(url, {method: "GET"})
            .then(response => response.json())
            .then(data => {
                this.setState({
                    addressBTC: data
                });
            })
            .catch(err => {});
    }

    copyValue(value) {
        var copyBtn = document.getElementById("copyAlertMsg");
        copyBtn.classList.add("gatewayBTC__copy-alert--fadeIn");

        navigator.clipboard.writeText(value);

        setTimeout(this.hideCopuAlert, 1400);
    }

    hideCopuAlert() {
        var copyBtn = document.getElementById("copyAlertMsg");
        copyBtn.classList.remove("gatewayBTC__copy-alert--fadeIn");
    }

    render() {
        let addressBTC = this.state.addressBTC;
        let userBalance = this.state.userBalance;

        return (
            <section className="cwd-common__wrap">
                <Link to="/gateway" className="cwdgateway__back-link">
                    <Translate
                        content="cwdgateway.back_btn"
                        className="cwd-common__btn"
                    />
                </Link>

                <div className="gatewayBTC__container">
                    <div className="gatewayBTC__inner">
                        <Translate
                            className="cwd-common__title"
                            content="cwdgateway.gateways.deposit__title"
                        />

                        <Translate
                            content="cwdgateway.gateways.deposit_text"
                            component="p"
                            className="cwd-common__description"
                        />

                        <div
                            className="gatewayBTC__address-wrap"
                            onClick={this.copyValue.bind(this, addressBTC)}
                        >
                            <span className="gatewayBTC__address">
                                {addressBTC}
                            </span>

                            <Icon
                                className="gatewayBTC__copy-btn"
                                size="1x"
                                name="public-key"
                            />

                            <div
                                id="copyAlertMsg"
                                className="gatewayBTC__copy-alert"
                            >
                                <Translate content="cwdgateway.gateways.copy_text" />
                            </div>
                        </div>
                    </div>

                    <div>
                        <Translate
                            className="cwd-common__title"
                            content="cwdgateway.gateways.withdraw__title"
                        />
                        {userBalance > 0 ? (
                            <TransferBTC transferAsset="1.3.3" />
                        ) : (
                            <div className="gatewayBTC__wrap">
                                <Translate
                                    className="cwd-common__description"
                                    content="modal.withdraw.no_BTC_balance"
                                />
                                <a
                                    className="gatewayBTC__link"
                                    href="https://crowdwiz.biz/market/CROWD.BTC_CWD"
                                >
                                    <Translate content="modal.withdraw.exchange_link" />
                                </a>
                            </div>
                        )}
                    </div>
                </div>
            </section>
        );
    }
}

export default GatewayBTC = connect(GatewayBTC, {
    listenTo() {
        return [AccountStore];
    },
    getProps() {
        if (AccountStore.getState().passwordAccount) {
            return {
                account: ChainStore.fetchFullAccount(
                    AccountStore.getState().passwordAccount
                )
            };
        }
    }
});
