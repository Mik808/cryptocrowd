import React from "react";
import Translate from "react-translate-component";
import Icon from "../Icon/Icon";
import {Link} from "react-router-dom";
import {connect} from "alt-react";
import AccountStore from "stores/AccountStore";
import {ChainStore} from "bitsharesjs";

//STYLES
import "./scss/cwdgateway-index.scss";

let headerImg = require("assets/icons/gateway_header.png");
let headerImgMobile = require("assets/icons/gateway_header_768.png");

class GatewayIndexPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentAccount: this.props.account
        };
    }
    render() {
        let gatewayData = [{gateway: "dex"}, {gateway: "btc"}];
        let currentAccount = this.state.currentAccount;
        let width = window.innerWidth;

        return (
            <section className="cwdgateway-index">
                <div className="cwdgateway-index__header">
                    <img
                        src={width > 768 ? headerImg : headerImgMobile}
                        alt="gateway"
                    />
                </div>

                <ul className="cwdgateway-index__cards">
                    {gatewayData.map((item, index) => (
                        <li key={index} className="cwdgateway-index__item">
                            {currentAccount ? (
                                <Link
                                    to={"gateway/" + item.gateway}
                                    className="cwdgateway-index__inner"
                                >
                                    <Translate
                                        className="cwdgateway-index__text"
                                        content={
                                            "cwdgateway.index." + item.gateway
                                        }
                                    />
                                    <Icon
                                        className="cwdgateway-index__icon"
                                        name={"gateway_" + item.gateway}
                                    />
                                </Link>
                            ) : (
                                <Link
                                    to="/create-account/password"
                                    className="cwdgateway-index__inner"
                                >
                                    <Translate
                                        className="cwdgateway-index__text"
                                        content={
                                            "cwdgateway.index." + item.gateway
                                        }
                                    />
                                    <Icon
                                        className="cwdgateway-index__icon"
                                        name={"gateway_" + item.gateway}
                                    />
                                </Link>
                            )}
                        </li>
                    ))}
                </ul>
            </section>
        );
    }
}

export default GatewayIndexPage = connect(GatewayIndexPage, {
    listenTo() {
        return [AccountStore];
    },
    getProps() {
        if (AccountStore.getState().passwordAccount) {
            return {
                account: ChainStore.fetchFullAccount(
                    AccountStore.getState().passwordAccount
                )
            };
        }
    }
});
