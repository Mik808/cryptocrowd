import React from "react";
import Translate from "react-translate-component";
import Icon from "../../../Icon/Icon";

class ReferralInfo extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showClipboardMsg: false
        };
    }

    async selectValue(ref) {
        try {
            await navigator.clipboard.writeText(ref);
            this.setState({["showClipboardMsg"]: true});
            setTimeout(() => {
                this.setState({["showClipboardMsg"]: false});
            }, 2000);
        } catch (err) {
            console.log(err, "text is not copied to clipboard");
        }
    }

    render() {
        let account_name = this.props.currentAccount;

        return (
            <div className="ref-info">
                <Translate
                    className="ref-info__text-block"
                    content="gamezone.matrix.ref_text"
                />
                <div
                    className="ref-info__link-block"
                    onClick={() =>
                        this.selectValue(
                            `https://crowdwiz.biz/?r=${account_name}`
                        )
                    }
                >
                    <div className="ref-info__link-text">{`https://crowdwiz.biz/?r=${account_name}`}</div>
                    <span className="ref-info__btn">
                        <Icon
                            className="ref-info__btn-img"
                            size="1x"
                            name="icon_copy"
                        />
                    </span>

                    {this.state.showClipboardMsg ? (
                        <div className="ref-info__msg">
                            <Translate content="account.vesting.copy_text" />
                        </div>
                    ) : null}
                </div>
            </div>
        );
    }
}
export default ReferralInfo;
