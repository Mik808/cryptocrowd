import React from "react";
import Translate from "react-translate-component";
import "react-sweet-progress/lib/style.css";
import Icon from "../../../Icon/Icon";

class MatrixBalances extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let totalCwdDeposit = this.props.totalCwdDeposit;
        let totalCwdWithdraw = this.props.totalCwdWithdraw;
        let balance = this.props.balance;
        let reload = this.props.reload;

        return (
            <ul className="matrix-balances__list">
                <li className="matrix-balances__item">
                    <div className="matrix-balances__inner">
                        <Icon
                            className="matrix-balances__icon"
                            name="icon_income"
                            title="gamezone.matrix.matrix_total_deposit"
                        />

                        <Translate
                            className="matrix-balances__text"
                            content="gamezone.matrix.matrix_total_deposit"
                        />

                        <span className="matrix-balances__data">
                            {totalCwdDeposit}
                        </span>
                    </div>
                </li>
                <li className="matrix-balances__item">
                    <div className="matrix-balances__inner">
                        <Icon
                            className="matrix-balances__icon"
                            name="icon_outcome"
                            title="gamezone.matrix.matrix_total_withdraw"
                        />

                        <Translate
                            className="matrix-balances__text"
                            content="gamezone.matrix.matrix_total_withdraw"
                        />

                        <span className="matrix-balances__data">
                            {totalCwdWithdraw}
                        </span>
                    </div>
                </li>
                <li className="matrix-balances__item">
                    <div className="matrix-balances__inner">
                        <Icon
                            className="matrix-balances__icon matrix-balances__icon--big"
                            name="icon_balance"
                            title="gamezone.matrix.matrix_balance"
                        />

                        <Translate
                            className="matrix-balances__text"
                            content="gamezone.matrix.matrix_balance"
                        />

                        <span className="matrix-balances__data">{balance}</span>
                    </div>
                </li>
                <li className="matrix-balances__item">
                    <div className="matrix-balances__inner">
                        <Icon
                            className="matrix-balances__icon matrix-balances__icon--big"
                            name="icon_reload"
                            title="gamezone.matrix.matrix_relaunch"
                        />

                        <Translate
                            className="matrix-balances__text"
                            content="gamezone.matrix.matrix_relaunch"
                        />

                        <span className="matrix-balances__data">
                            {reload} %
                        </span>
                    </div>
                </li>
            </ul>
        );
    }
}
export default MatrixBalances;
