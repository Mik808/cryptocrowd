import React from "react";
import Translate from "react-translate-component";
import WalletUnlockActions from "actions/WalletUnlockActions";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import WalletDb from "stores/WalletDb";
import {ChainStore} from "bitsharesjs";
import {Apis} from "bitsharesjs-ws";
import LinkToAccountById from "../Utility/LinkToAccountById";
import {RecentTransactions} from "../Account/RecentTransactions";
import {connect} from "alt-react";
import Icon from "../Icon/Icon";
import {Tabs, Tab} from "../Utility/Tabs";
import counterpart from "counterpart";
import {FormattedNumber} from "react-intl";
import TransactionConfirmStore from "stores/TransactionConfirmStore";
import SettingsStore from "stores/SettingsStore";

//STYLES
import "./scss/game-heads.scss";

class GameHeads extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            flipcoin: [],
            betAmount: 0,
            showModal: false,
            locales: SettingsStore.getState().defaults.locale,
            currentLocale: SettingsStore.getState().settings.get("locale"),
            width: window.innerWidth,
            intervalID: 0
        };

        this.getBetsArray = this.getBetsArray.bind(this);
        this.makeBet = this.makeBet.bind(this);
        this.callBet = this.callBet.bind(this);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    componentDidMount() {
        this.getBetsArray();
        this.updateWindowDimensions();
        window.addEventListener("resize", this.updateWindowDimensions);

        this.setState({
            intervalID: setInterval(this.getBetsArray.bind(this), 5000)
        });
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateWindowDimensions);
        clearInterval(this.state.intervalID);
    }

    updateWindowDimensions() {
        this.setState({width: window.innerWidth, height: window.innerHeight});
    }

    getBetsArray() {
        Apis.instance()
            .db_api()
            .exec("get_active_flipcoin", [])
            .then(flipcoin => {
                this.setState({
                    flipcoin: flipcoin
                });
            })
            .catch(err => {
                console.log("error:", err);
            });
    }

    handleBetAmount(event) {
        this.setState({betAmount: event.target.value});
    }

    makeBet(amount, e) {
        e.preventDefault();
        amount = Math.floor(parseFloat(amount));
        if (this.props.account) {
            if (WalletDb.isLocked()) {
                WalletUnlockActions.unlock()
                    .then(() => {
                        AccountActions.tryToSetCurrentAccount();
                    })
                    .catch(() => {});

                let bettor = this.props.account.get("id");
                let bet = amount;
                if (bet <= 0) {
                    alert("Ставка должна быть больше нуля");
                } else {
                    AccountActions.flipcoinBet(bettor, bet)
                        .then(() => {
                            TransactionConfirmStore.unlisten(
                                this.onTrxIncluded
                            );
                            TransactionConfirmStore.listen(this.onTrxIncluded);
                        })
                        .then(this.setState({showModal: false}))
                        .catch(err => {
                            console.log("error:", err);
                        });
                }
            } else {
                let bettor = this.props.account.get("id");
                let bet = amount;
                if (bet <= 0) {
                    alert("Ставка должна быть больше нуля");
                } else {
                    AccountActions.flipcoinBet(bettor, bet)
                        .then(() => {
                            TransactionConfirmStore.unlisten(
                                this.onTrxIncluded
                            );
                            TransactionConfirmStore.listen(this.onTrxIncluded);
                        })
                        .then(this.setState({showModal: false}))
                        .catch(err => {
                            console.log("error:", err);
                        });
                }
            }
        } else {
            this.props.history.push("/create-account/password");
        }
    }

    callBet(flipcoin_, amount, e) {
        e.preventDefault();
        if (this.props.account) {
            if (WalletDb.isLocked()) {
                WalletUnlockActions.unlock()
                    .then(() => {
                        AccountActions.tryToSetCurrentAccount();
                    })
                    .catch(() => {});

                let caller = this.props.account.get("id");
                let bet = amount;
                let flipcoin = flipcoin_;
                AccountActions.flipcoinCall(flipcoin, caller, bet)
                    .then(() => {
                        TransactionConfirmStore.unlisten(this.onTrxIncluded);
                        TransactionConfirmStore.listen(this.onTrxIncluded);
                    })
                    .catch(err => {
                        console.log("error:", err);
                    });
            } else {
                let caller = this.props.account.get("id");
                let bet = amount;
                let flipcoin = flipcoin_;
                AccountActions.flipcoinCall(flipcoin, caller, bet)
                    .then(() => {
                        TransactionConfirmStore.unlisten(this.onTrxIncluded);
                        TransactionConfirmStore.listen(this.onTrxIncluded);
                    })
                    .catch(err => {
                        console.log("error:", err);
                    });
            }
        } else {
            this.props.history.push("/create-account/password");
        }
    }

    toggleModal(e) {
        this.setState({showModal: !this.state.showModal});
    }

    hideModal(e) {
        e.preventDefault();
        if (e.target === e.currentTarget) {
            this.setState({showModal: !this.state.showModal});
        }
    }

    render() {
        let flipCoin = this.state.flipcoin;

        let gameHeadsLogo =
            this.state.currentLocale === "ru"
                ? require("assets/img/heads-or-tails_3.png")
                : this.state.width > 600
                ? require("assets/img/HeadsOrTailsEng-2x.png")
                : require("assets/img/HeadsOrTailsEng.png");

        const modal = (
            <div>
                <div className="ant-modal-mask"></div>

                <div
                    className="game-heads__modal"
                    onClick={e => this.hideModal(e)}
                >
                    <div className="game-heads__modal-content">
                        <Icon
                            size="1x"
                            name="cross"
                            className="game-heads__modal-close"
                            onClick={() => this.toggleModal()}
                        />
                        <form>
                            <div className="game-heads__modal-input-wrapper">
                                <input
                                    className="game-heads__modal-input"
                                    placeholder={counterpart.translate(
                                        "gamezone.enter-amount"
                                    )}
                                    type="number"
                                    min="0.01"
                                    id="userBet"
                                    onChange={event =>
                                        this.handleBetAmount(event)
                                    }
                                />
                                <input
                                    className="game-heads__modal-input-currency"
                                    type="text"
                                    value="CWD"
                                    disabled
                                />
                            </div>
                            <div className="game-heads__modal-btn-wrap">
                                <Translate
                                    className="common-btn"
                                    content="gamezone.bet"
                                    component="button"
                                    onClick={e =>
                                        this.makeBet(
                                            this.state.betAmount * 100000,
                                            e
                                        )
                                    }
                                />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );

        return (
            <div className="game-heads__wrap">
                <img className="game-heads__logo" src={gameHeadsLogo} alt="" />
                <div className="game-heads__content">
                    <div className="game-heads__bets-wrap">
                        <div className="game-heads__desc-wrap">
                            <Translate
                                className="game-heads__description"
                                content="gamezone.heads-desc-01"
                                component="p"
                            />
                        </div>

                        <Translate
                            className="game-heads__text"
                            content="gamezone.heads-text"
                            component="p"
                        />
                        <div className="game-heads__list">
                            <button
                                className="game-heads__btn noselect"
                                onClick={this.makeBet.bind(this, 1 * 100000)}
                            >
                                <span className="game-heads__amount">
                                    1 CWD
                                </span>
                                <span className="game-heads__coin game-heads__coin--5" />
                            </button>
                            <button
                                className="game-heads__btn noselect"
                                onClick={this.makeBet.bind(this, 3 * 100000)}
                            >
                                <span className="game-heads__amount">
                                    3 CWD
                                </span>
                                <span className="game-heads__coin game-heads__coin--10" />
                            </button>
                            <button
                                className="game-heads__btn noselect"
                                onClick={this.makeBet.bind(this, 5 * 100000)}
                            >
                                <span className="game-heads__amount">
                                    5 CWD
                                </span>
                                <span className="game-heads__coin game-heads__coin--25" />
                            </button>
                            <button
                                className="game-heads__btn noselect"
                                onClick={this.makeBet.bind(this, 10 * 100000)}
                            >
                                <span className="game-heads__amount">
                                    10 CWD
                                </span>
                                <span className="game-heads__coin game-heads__coin--50" />
                            </button>

                            {this.state.showModal ? modal : null}

                            <div className="game-heads__form">
                                <button
                                    className="game-heads__btn noselect user-btn"
                                    onClick={this.toggleModal.bind(this)}
                                >
                                    <Translate
                                        className="game-heads__amount game-heads__amount--custom"
                                        component="span"
                                        content="gamezone.custom-bet"
                                    />
                                    {this.state.showAmountInput ? (
                                        <input
                                            type="number"
                                            placeholder={counterpart.translate(
                                                "gamezone.enter-amount"
                                            )}
                                        />
                                    ) : (
                                        ""
                                    )}
                                    <span className="game-heads__coin game-heads__coin--custom" />
                                </button>
                            </div>
                        </div>
                    </div>

                    {flipCoin.length > 0 ? (
                        <div className="game-heads__bets">
                            <Translate
                                className="heads-bet__title"
                                content="gamezone.active-bets"
                                component="p"
                            />
                            <ul className="heads-bet__list">
                                <li className="heads-bet__head">
                                    <div className="heads-bet__column">
                                        <Translate
                                            component="span"
                                            content="gamezone.player"
                                        />
                                    </div>
                                    <div className="heads-bet__column heads-bet__column--time">
                                        <Translate
                                            component="span"
                                            content="gamezone.date-time"
                                        />
                                    </div>
                                    <div className="heads-bet__column">
                                        <Translate
                                            component="span"
                                            content="gamezone.bet-amount"
                                        />
                                    </div>
                                    <div className="heads-bet__column">
                                        <Translate
                                            component="span"
                                            content="gamezone.call-bet-title"
                                        />
                                    </div>
                                </li>
                                {flipCoin.map(bet => (
                                    <li
                                        className="heads-bet__row"
                                        key={bet["id"]}
                                    >
                                        <div className="heads-bet__column">
                                            <LinkToAccountById
                                                account={bet["bettor"]}
                                            />
                                        </div>
                                        <div className="heads-bet__date heads-bet__column heads-bet__column--time">
                                            {bet.expiration}
                                        </div>
                                        <div className="heads-bet__column">
                                            <span className="heads-bet__amount">
                                                {bet["bet"]["amount"] / 100000}
                                                &nbsp;
                                            </span>
                                            <span className="heads-bet__asset">
                                                CWD
                                            </span>
                                        </div>
                                        <div
                                            className="heads-bet__column"
                                            onClick={this.callBet.bind(
                                                this,
                                                bet["id"],
                                                bet["bet"]["amount"]
                                            )}
                                        >
                                            <div className="heads-bet__reply">
                                                <Translate
                                                    className="heads-bet__reply-text"
                                                    component="span"
                                                    content="gamezone.call-bet"
                                                />
                                                <Icon
                                                    size="1x"
                                                    name="hand_btn"
                                                    title="gamezone.call-bet"
                                                    className="heads-bet__icon"
                                                />
                                            </div>
                                        </div>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    ) : null}
                    <div className="game-heads__bets">
                        {this.props.account ? (
                            <div className="game-heads__trn-wrap">
                                <Translate
                                    className="game-heads__text"
                                    content="gamezone.bet-history"
                                    component="span"
                                />
                                <RecentTransactions
                                    accountsList={[
                                        this.props.account.get("id")
                                    ]}
                                    compactView={false}
                                    showMore={true}
                                    fullHeight={true}
                                    limit={10}
                                    showFilters={false}
                                    dashboard
                                    multiFilter={[50, 51, 52, 53, 54]}
                                    gamezoneView={true}
                                />
                            </div>
                        ) : null}
                    </div>
                </div>
            </div>
        );
    }
}
export default GameHeads = connect(GameHeads, {
    listenTo() {
        return [AccountStore];
    },
    getProps() {
        if (AccountStore.getState().passwordAccount) {
            return {
                account: ChainStore.fetchFullAccount(
                    AccountStore.getState().passwordAccount
                )
            };
        }
    }
});
