﻿import React from "react";
import Translate from "react-translate-component";
import {Link} from "react-router-dom";

//STYLES
import "./scss/gamezone.scss";

let headerImg = require("assets/icons/gamezone_logo.png");

class GameZone extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="gamezone__wrap">
                <div className="gamezone__center-layout">
                    <div className="gamezone__header-wrap">
                        <img
                            className="gamezone__header"
                            src={headerImg}
                            alt=""
                        />
                    </div>

                    <Translate
                        className="gamezone__title"
                        content="gamezone.title"
                        component="h1"
                    />

                    <div className="gamezone__games-wrap">
                        {/* MATRIX POSTER */}
                        <div className="gamezone-matrix">
                            <div className="gamezone-matrix__inner">
                                <Translate
                                    className="gamezone-matrix__intro"
                                    content="gamezone.matrix-intro"
                                />
                                <Link
                                    className="game-item__btn game-item__btn--matrix noselect"
                                    to={`gamezone/matrix-game`}
                                >
                                    <Translate
                                        className="game-item__btn-text"
                                        content="gamezone.matrix-btn"
                                        component="span"
                                    />
                                </Link>
                            </div>
                        </div>
                        {/* MATRIX POSTER */}

                        <ul className="game-item__list">
                            <li className="game-item">
                                <div className="game-item__content game-item__content--lottery">
                                    <div className="game-item__inner">
                                        <Translate
                                            className="game-item__title"
                                            content="gamezone.game_lottery"
                                            component="p"
                                        />

                                        <Link
                                            className="game-item__btn noselect"
                                            to={`gamezone/scoop`}
                                        >
                                            <Translate
                                                className="game-item__btn-text"
                                                content="gamezone.buy-ticket"
                                                component="span"
                                            />
                                        </Link>
                                    </div>
                                </div>
                            </li>
                            <li className="game-item">
                                <div className="game-item__content game-item__content--heads">
                                    <div className="game-item__inner">
                                        <Translate
                                            className="game-item__title"
                                            content="gamezone.game_heads"
                                            component="p"
                                        />

                                        <Link
                                            className="game-item__btn noselect"
                                            to={`gamezone/heads-or-tails`}
                                        >
                                            <Translate
                                                className="game-item__btn-text"
                                                content="gamezone.bet"
                                                component="span"
                                            />
                                        </Link>
                                    </div>
                                </div>
                            </li>
                            {/* <li className="game-item">
                            <Translate
                                className="game-item__label"
                                content="gamezone.soon"
                                component="span"
                            />
                            <div className="game-item__content game-item__content--inactive game-item__content--dices">
                                <div className="game-item__inner">
                                    <Translate
                                        className="game-item__title"
                                        content="gamezone.game_dices"
                                        component="p"
                                    />

                                    <Link
                                        className="game-item__btn noselect"
                                        to={`gamezone`}
                                    >
                                        <Translate
                                            className="game-item__btn-text"
                                            content="gamezone.bet"
                                            component="span"
                                        />
                                    </Link>
                                </div>
                            </div>
                        </li> */}
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}
export default GameZone;
