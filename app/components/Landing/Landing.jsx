import React from "react";
import {connect} from "alt-react";
import {ChainStore} from "bitsharesjs";
import AccountStore from "stores/AccountStore";
import LandHeader from "./components/LandHeader";
import Blockchain from "./components/Blockchain";
import ProductsBlock from "./components/ProductsBlock";
import StoriesBlock from "./components/StoriesBlock";
import SmiBlock from "./components/SmiBlock";
import BusinessBlock from "./components/BusinessBlock";
import FeaturesBlock from "./components/FeaturesBlock";
import EventsBlock from "./components/EventsBlock";
import CommunityBlock from "./components/CommunityBlock";
import Infinity from "./components/Infinity";
import LandFooter from "./components/LandFooter";

//STYLES
import "./scss/index.scss";
import "./scss/index-header.scss";
import "./scss/lang-select.scss";
import "./scss/mission.scss";
import "./scss/cwd-blockchain.scss";
import "./scss/products.scss";
import "./scss/stories-block.scss";
import "./scss/smi-block.scss";
import "./scss/business.scss";
import "./scss/features.scss";
import "./scss/events.scss";
import "./scss/community.scss";
import "./scss/infinity.scss";
import "./scss/index-footer.scss";

class Index extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="indexMain">
                {/* HEADER BLOCK */}
                <LandHeader
                    account={this.props.account}
                    history={this.props.history}
                />

                {/* CWD-BLOCKCHAIN BLOCK */}
                <Blockchain />

                {/*SMI BLOCK*/}
                <SmiBlock />

                {/*STORIES BLOCK*/}
                <StoriesBlock />

                {/* BUSINESS BLOCK */}
                <BusinessBlock />

                {/*PRODUCTS BLOCK */}
                <ProductsBlock currentAccount={this.props.account} />

                {/* FEATURES BLOCK */}
                <FeaturesBlock />

                {/* EVENTS BLOCK */}
                <EventsBlock />

                {/* COMMUNITY BLOCK */}
                <CommunityBlock />

                {/* INFINITY BLOCK */}
                <Infinity />

                {/* FOOTER*/}
                <LandFooter />
            </div>
        );
    }
}

export default Index = connect(Index, {
    listenTo() {
        return [AccountStore];
    },
    getProps() {
        if (AccountStore.getState().passwordAccount) {
            return {
                account: ChainStore.fetchFullAccount(
                    AccountStore.getState().currentAccount ||
                        AccountStore.getState().passwordAccount
                )
            };
        }
    }
});
