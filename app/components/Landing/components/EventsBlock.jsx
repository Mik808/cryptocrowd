import React from "react";
import Translate from "react-translate-component";
import {Link} from "react-router-dom";

class EventsBlock extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <section className="events">
                <div className="events__wrap index-center-wrap">
                    <Link
                        className="events__btn index-btn noselect"
                        to={`/create-account/password`}
                    >
                        <Translate
                            content="landing.events.btn"
                            className="index-btn__text"
                        />
                    </Link>

                    <Translate
                        content="landing.events.title"
                        className="events__title block-title"
                        component="div"
                    />

                    <div className="events__album">
                        <div className="events__left-column">
                            <div className="events__img-wrap">
                                <img
                                    src="https://crowdwiz.biz/ipfs/QmZRxRpaPa6hjxopinbtyNWTf82ENXHfuUh7dC2zxSQMaj/events/adler.jpg"
                                    alt=""
                                />
                                <Translate
                                    content="landing.events.city-01"
                                    className="events__city-name"
                                />
                            </div>
                        </div>
                        <div className="events__right-column">
                            <div className="events__row events__row--top">
                                <div className="events__row-img">
                                    <img
                                        src="https://crowdwiz.biz/ipfs/QmZRxRpaPa6hjxopinbtyNWTf82ENXHfuUh7dC2zxSQMaj/events/moscow.jpg"
                                        alt=""
                                    />
                                    <Translate
                                        content="landing.events.city-02"
                                        className="events__city-name"
                                    />
                                </div>
                                <div className="events__row-img events__row-img--square">
                                    <img
                                        src="https://crowdwiz.biz/ipfs/QmZRxRpaPa6hjxopinbtyNWTf82ENXHfuUh7dC2zxSQMaj/events/sochi_4.jpg"
                                        alt=""
                                    />
                                    <Translate
                                        content="landing.events.city-03"
                                        className="events__city-name"
                                    />
                                </div>
                            </div>
                            <div className="events__row">
                                <div className="events__row-img events__row-img--square">
                                    <img
                                        src="https://crowdwiz.biz/ipfs/QmZRxRpaPa6hjxopinbtyNWTf82ENXHfuUh7dC2zxSQMaj/events/moscow.png"
                                        alt=""
                                    />
                                    <Translate
                                        content="landing.events.city-02"
                                        className="events__city-name"
                                    />
                                </div>
                                <div className="events__row-img">
                                    <img
                                        src="https://crowdwiz.biz/ipfs/QmZRxRpaPa6hjxopinbtyNWTf82ENXHfuUh7dC2zxSQMaj/events/sochi.jpg"
                                        alt=""
                                    />
                                    <Translate
                                        content="landing.events.city-03"
                                        className="events__city-name"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default EventsBlock;
