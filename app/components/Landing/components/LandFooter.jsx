import React from "react";
import Translate from "react-translate-component";

class LandFooter extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        let width = window.innerWidth;

        return (
            <footer className="index-footer">
                <div className="infinity__wrap index-center-wrap">
                    <ul className="index-footer__social-block">
                        <li className="index-footer__social-item">
                            <a
                                href="https://vk.com/crowdwiz"
                                className="index-footer__icon index-footer__icon--vk"
                                target="_blank"
                            />
                        </li>
                        <li className="index-footer__social-item">
                            <a
                                href="https://www.instagram.com/crowdwiz.biz/"
                                className="index-footer__icon index-footer__icon--insta"
                                target="_blank"
                            />
                        </li>
                        <li className="index-footer__social-item">
                            <a
                                href={
                                    width < 567
                                        ? "https://t.me/joinchat/IpAvyVKGfngwGOyKPrjTSw"
                                        : "https://tgme.pro/joinchat/IpAvyVKGfngwGOyKPrjTSw"
                                }
                                className="index-footer__icon index-footer__icon--tel"
                                target="_blank"
                            />
                        </li>
                        <li className="index-footer__social-item">
                            <a
                                href="https://crowdwiz.biz/ipfs/QmRVaM6SpK8YjL6TRRMZo5pSy6nkQqMHzteyEc528YsaLA"
                                className="index-footer__icon index-footer__icon--wp"
                                download
                                target="_blank"
                            />
                        </li>
                    </ul>

                    <div className="index-footer__text-wrap">
                        &copy;
                        <Translate
                            content="landing.footer.off-text"
                            className="index-footer__off-text"
                        />
                    </div>
                </div>
            </footer>
        );
    }
}

export default LandFooter;
