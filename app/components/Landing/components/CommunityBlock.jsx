import React from "react";
import Translate from "react-translate-component";

class CommunityBlock extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <section className="community">
                <div className="community__wrap index-center-wrap">
                    <Translate
                        content="landing.community.title"
                        className="community__title block-title"
                        component="div"
                    />
                    <ul className="community__list">
                        <li className="community__item">
                            <div className="community__inner">
                                <span className="community__icon community__icon--01" />
                                <Translate
                                    content="landing.community.text-01"
                                    className="community__text"
                                />
                            </div>
                        </li>
                        <li className="community__item">
                            <div className="community__inner">
                                <span className="community__icon community__icon--02" />
                                <Translate
                                    content="landing.community.text-02"
                                    className="community__text"
                                />
                            </div>
                        </li>
                        <li className="community__item">
                            <div className="community__inner">
                                <span className="community__icon community__icon--03" />
                                <Translate
                                    content="landing.community.text-03"
                                    className="community__text"
                                />
                            </div>
                        </li>
                        <li className="community__item">
                            <div className="community__inner">
                                <span className="community__icon community__icon--04" />
                                <Translate
                                    content="landing.community.text-04"
                                    className="community__text"
                                />
                            </div>
                        </li>
                        <li className="community__item">
                            <div className="community__inner">
                                <span className="community__icon community__icon--05" />
                                <Translate
                                    content="landing.community.text-05"
                                    className="community__text"
                                />
                            </div>
                        </li>
                        <li className="community__item">
                            <div className="community__inner">
                                <span className="community__icon community__icon--06" />
                                <Translate
                                    content="landing.community.text-06"
                                    className="community__text"
                                />
                            </div>
                        </li>
                    </ul>
                </div>
            </section>
        );
    }
}

export default CommunityBlock;
