import React from "react";
import Translate from "react-translate-component";
import {Link} from "react-router-dom";

let logo = require("assets/icons/logo-index.png");
let logoMob = require("assets/icons/logo-index-mob.png");
let arrow = require("assets/icons/business-arrow.png");

class BusinessBlock extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        let width = window.innerWidth;
        let cwdLogo;

        if (width > 420) {
            cwdLogo = logo;
        } else {
            cwdLogo = logoMob;
        }

        return (
            <section className="business">
                <div className="business__wrap index-center-wrap">
                    <Link
                        className="business__btn index-btn noselect"
                        to={`/create-account/password`}
                    >
                        <Translate
                            content="landing.business.btn"
                            className="index-btn__text"
                        />
                    </Link>

                    <div className="business__inner">
                        <img
                            className="business__logo"
                            src={cwdLogo}
                            alt="crowdwiz"
                        />

                        <Translate
                            content="landing.business.title"
                            className="business__title"
                        />

                        <Translate
                            content="landing.business.subtitle"
                            className="business__subtitle"
                        />
                    </div>

                    <div className="business__arrow-wrap">
                        <img
                            src={arrow}
                            className="business__arrow-img"
                            alt="details"
                        />
                        <Translate
                            content="landing.business.details"
                            className="business__details"
                        />
                    </div>

                    <div className="business-instructions">
                        <Translate
                            content="landing.business.instructions-title"
                            className="business-instructions__title"
                        />
                        <Translate
                            content="landing.business.instructions-intro"
                            className="business-instructions__intro"
                        />
                    </div>

                    <ul className="business-instructions__list">
                        <li className="business-instructions__item">
                            <span className="business-instructions__num business-instructions__num--01" />
                            <Translate
                                content="landing.business.instructions-text-01"
                                className="business-instructions__text"
                            />
                        </li>
                        <li className="business-instructions__item">
                            <span className="business-instructions__num business-instructions__num--02" />
                            <Translate
                                content="landing.business.instructions-text-02"
                                className="business-instructions__text"
                            />
                        </li>
                        <li className="business-instructions__item">
                            <span className="business-instructions__num business-instructions__num--03" />
                            <Translate
                                content="landing.business.instructions-text-03"
                                className="business-instructions__text"
                            />
                        </li>
                        <li className="business-instructions__item">
                            <span className="business-instructions__num business-instructions__num--04" />
                            <Translate
                                content="landing.business.instructions-text-04"
                                className="business-instructions__text"
                            />
                        </li>
                        <li className="business-instructions__item">
                            <span className="business-instructions__num business-instructions__num--05" />
                            <Translate
                                content="landing.business.instructions-text-05"
                                className="business-instructions__text"
                            />
                        </li>
                        <li className="business-instructions__item">
                            <span className="business-instructions__num business-instructions__num--06" />
                            <Translate
                                content="landing.business.instructions-text-06"
                                className="business-instructions__text"
                            />
                        </li>
                    </ul>
                </div>
            </section>
        );
    }
}

export default BusinessBlock;
