import React from "react";
import Translate from "react-translate-component";
import {Link} from "react-router-dom";
import WalletUnlockActions from "actions/WalletUnlockActions";
import AccountActions from "actions/AccountActions";
import SettingsStore from "stores/SettingsStore";
import counterpart from "counterpart";
import IntlActions from "actions/IntlActions";
import SettingsActions from "actions/SettingsActions";
import Icon from "../../Icon/Icon";

// let logo = require("assets/icons/logo-christmas.png");
// let logoMobile = require("assets/icons/logo-christmas-mob.png");

class LandHeader extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            account: this.props.account,
            locales: SettingsStore.getState().defaults.locale,
            currentLocale: SettingsStore.getState().settings.get("locale"),
            isSelectOpened: false
        };
        this.handleLanguageSelect = this.handleLanguageSelect.bind(this);
        this.opnenSelect = this.opnenSelect.bind(this);
    }

    logIn(e) {
        e.preventDefault();
        WalletUnlockActions.unlock()
            .then(() => {
                AccountActions.tryToSetCurrentAccount();
                this.props.history.push(
                    "/account/" + this.props.account.get("name")
                );
            })
            .catch(() => {});
    }

    opnenSelect() {
        this.setState({
            isSelectOpened: true
        });
    }

    handleLanguageSelect(lang) {
        IntlActions.switchLocale(lang);
        SettingsActions.changeSetting({
            setting: "locale",
            value: lang
        });

        var elems = document.getElementsByClassName("lang-select__item");
        for (var i = 0; i < elems.length; i++) {
            elems[i].classList.remove("lang-select__item--hidden");
        }

        this.setState({
            currentLocale: lang,
            isSelectOpened: false
        });
    }

    render() {
        let account = this.state.account;
        let langImg = {};
        let currentLocale = this.state.currentLocale;
        let isSelectOpened = this.state.isSelectOpened;
        let width = window.innerWidth;

        this.state.locales.map(
            locale =>
                (langImg[locale] = require("assets/icons/lang_" +
                    locale +
                    ".png"))
        );

        return (
            <header className="index-header">
                <div className="index-header__animation" />
                <div className="index-header__inner index-center-wrap">
                    <div
                        className={
                            account
                                ? "index-header__auth"
                                : "index-header__auth index-header__auth--non-reg"
                        }
                    >
                        <div
                            className={
                                account
                                    ? "index-header__locale"
                                    : "index-header__locale index-header__locale--non-reg"
                            }
                        >
                            {isSelectOpened ? (
                                <ul
                                    className="lang-select__list"
                                    value={this.state.currentLocale}
                                >
                                    {this.state.locales.map(locale => (
                                        <li
                                            key={locale}
                                            value={locale}
                                            onClick={this.handleLanguageSelect.bind(
                                                this,
                                                locale
                                            )}
                                            className="lang-select__item"
                                        >
                                            <img
                                                className="lang-select__img"
                                                src={langImg[locale]}
                                                alt=""
                                            />
                                            <span className="lang-select__text">
                                                {counterpart.translate(
                                                    "languages." + locale
                                                )}
                                            </span>
                                        </li>
                                    ))}
                                </ul>
                            ) : (
                                <div
                                    className="lang-select__selector"
                                    onClick={this.opnenSelect.bind()}
                                >
                                    <div className="lang-select__item lang-select__item--closed">
                                        <img
                                            className="lang-select__img"
                                            src={langImg[currentLocale]}
                                            alt=""
                                        />
                                        <span className="lang-select__text">
                                            {counterpart.translate(
                                                "languages." + currentLocale
                                            )}
                                        </span>
                                    </div>
                                    <i className="lang-select__marker"></i>
                                </div>
                            )}
                        </div>

                        <div className="index-header__reg-wrap">
                            {width > 576 && !account ? (
                                <Link
                                    className="index-header__reg noselect"
                                    to={`/create-account/password`}
                                >
                                    <Translate content="registration.title" />
                                </Link>
                            ) : null}

                            <div
                                className="index-header__login"
                                onClick={e => {
                                    this.logIn(e);
                                }}
                            >
                                <span className="index-header__login-img"></span>
                                <Translate
                                    className="index-header__login-text"
                                    content="landing.header.login-btn"
                                />
                            </div>
                        </div>

                        {width < 576 && !account ? (
                            <Link
                                className="index-header__reg noselect"
                                to={`/create-account/password`}
                            >
                                <Translate content="registration.title" />
                            </Link>
                        ) : null}
                    </div>
                    <Icon name="logo-index" className="index-header__logo" />

                    <Translate
                        content="landing.header.description"
                        component="p"
                        className="index-header__title"
                    />
                    <Translate
                        content="landing.header.slogan"
                        component="div"
                        className="index-header__subtitle"
                    />

                    {/* MISSION BLOCK */}
                    <div className="mission">
                        <div className="mission__head">
                            <Translate
                                content="landing.mission.title"
                                className="mission__title block-title"
                            />
                            <Translate
                                content="landing.mission.goal"
                                component="p"
                                className="mission__subtitle"
                            />
                        </div>
                        <div className="mission__body">
                            <div className="mission__description-wrapper">
                                <div className="mission__num" />
                                <Translate
                                    content="landing.mission.text-01"
                                    component="div"
                                    className="mission__description"
                                />
                            </div>
                            <div className="mission__description-wrapper">
                                <div className="mission__num" />
                                <Translate
                                    content="landing..mission.text-02"
                                    component="div"
                                    className="mission__description"
                                />
                            </div>
                            <div className="mission__description-wrapper">
                                <div className="mission__num" />
                                <Translate
                                    content="landing..mission.text-03"
                                    component="div"
                                    className="mission__description"
                                />
                            </div>
                            <div className="mission__description-wrapper">
                                <div className="mission__num" />
                                <Translate
                                    content="landing..mission.text-04"
                                    component="div"
                                    className="mission__description"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}

export default LandHeader;
