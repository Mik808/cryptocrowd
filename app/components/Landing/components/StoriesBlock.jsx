import React from "react";
import Translate from "react-translate-component";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import counterpart from "counterpart";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

class StoriesBlock extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let storiesImg = [
            {
                img: "02.jpg",
                name: counterpart.translate("landing.stories-block.names.02"),
                description: counterpart.translate(
                    "landing.stories-block.info.02"
                )
            },
            {
                img: "03.jpg",
                name: counterpart.translate("landing.stories-block.names.03"),
                description: counterpart.translate(
                    "landing.stories-block.info.03"
                )
            },

            {
                img: "04.jpg",
                name: counterpart.translate("landing.stories-block.names.04"),
                description: counterpart.translate(
                    "landing.stories-block.info.04"
                )
            },
            {
                img: "06.jpg",
                name: counterpart.translate("landing.stories-block.names.06"),
                description: counterpart.translate(
                    "landing.stories-block.info.06"
                )
            },
            {
                img: "09.jpg",
                name: counterpart.translate("landing.stories-block.names.09"),
                description: counterpart.translate(
                    "landing.stories-block.info.09"
                )
            },
            {
                img: "10.jpg",
                name: counterpart.translate("landing.stories-block.names.10"),
                description: counterpart.translate(
                    "landing.stories-block.info.10"
                )
            }
        ];

        let width = window.innerWidth;
        let sliderItems;
        let autoplay;

        if (width <= 768) {
            sliderItems = 30;
            if (width <= 580) {
                sliderItems = 100;
                autoplay = false;
            }
        } else {
            sliderItems = 16;
        }

        let settings = {
            slidesToShow: 6,
            slidesToScroll: 1,
            autoPlay: true,
            responsive: [
                {
                    breakpoint: 1150,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        };

        return (
            <section className="stories-block">
                <div className="stories-block__wrap index-center-wrap">
                    <div className="stories-block__header">
                        <Translate
                            content="landing.stories-block.title"
                            className="stories-block__title block-title"
                            component="div"
                        />
                        <div className="stories-block__scroll-wrap">
                            <Translate
                                content="landing.stories-block.sсroll-text"
                                className="stories-block__scroll-text"
                            />
                        </div>
                    </div>
                </div>
                <div className="stories-block__gallery-block">
                    <Slider {...settings}>
                        {storiesImg.map((img, index) => (
                            <div key={index}>
                                <img
                                    src={
                                        "https://crowdwiz.biz/ipfs/QmZRxRpaPa6hjxopinbtyNWTf82ENXHfuUh7dC2zxSQMaj/stories/stories_" +
                                        img.img
                                    }
                                />
                                <p className="stories-block__name legend">
                                    {img.name}
                                </p>
                                <span className="stories-block__info">
                                    {img.description}
                                </span>
                            </div>
                        ))}
                    </Slider>
                </div>
            </section>
        );
    }
}

export default StoriesBlock;
