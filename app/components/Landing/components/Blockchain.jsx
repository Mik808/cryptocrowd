import React from "react";
import Translate from "react-translate-component";
import Lottie from "react-lottie";
import * as animationData from "../animation/speedometer.json";

class Blockchain extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isStopped: false,
            isPaused: false
        };
    }

    render() {
        const defaultOptions = {
            renderer: "svg",
            loop: true,
            autoplay: true,
            animationData: animationData.default,
            rendererSettings: {
                preserveAspectRatio: "xMidYMid slice"
            }
        };

        return (
            <section className="cwd-blockchain">
                <div className="index-center-wrap">
                    <Translate
                        content="landing.blockchain.title"
                        className="cwd-blockchain__title block-title"
                    />
                    <div className="cwd-blockchain__speedometer" id="lottie">
                        {/* <Lottie
                            options={defaultOptions}
                            isStopped={this.state.isStopped}
                            isPaused={this.state.isPaused}
                        /> */}
                    </div>
                    <ul className="cwd-blockchain__list">
                        <Translate
                            content="landing.blockchain.DPOS"
                            component="li"
                            className="cwd-blockchain__item cwd-blockchain__item--01"
                        />
                        <Translate
                            content="landing.blockchain.cloud_tech"
                            component="li"
                            className="cwd-blockchain__item cwd-blockchain__item--02"
                        />
                        <Translate
                            content="landing.blockchain.decentralization"
                            component="li"
                            className="cwd-blockchain__item cwd-blockchain__item--03"
                        />
                        <Translate
                            content="landing.blockchain.user_safety"
                            component="li"
                            className="cwd-blockchain__item cwd-blockchain__item--04"
                        />
                        <Translate
                            content="landing.blockchain.flexible_api"
                            component="li"
                            className="cwd-blockchain__item cwd-blockchain__item--05"
                        />
                        <Translate
                            content="landing.blockchain.high_speed"
                            component="li"
                            className="cwd-blockchain__item cwd-blockchain__item--06"
                        />

                        <Translate
                            content="landing.blockchain.crowdwizdom"
                            component="li"
                            className="cwd-blockchain__item cwd-blockchain__item--07"
                        />
                    </ul>
                    <span className="cwd-blockchain__line" />
                </div>
            </section>
        );
    }
}

export default Blockchain;
