import React from "react";
import Translate from "react-translate-component";

class FeaturesBlock extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <section className="features">
                <div className="index-center-wrap">
                    <ul className="features__list">
                        <li className="features__item features__item--title">
                            <Translate
                                content="landing.features.title"
                                className="features__title block-title"
                            />
                        </li>
                        <li className="features__item">
                            <div className="features__inner">
                                <span className="features__icon features__icon--01" />
                                <Translate
                                    content="landing.features.text-01"
                                    className="features__text"
                                />
                            </div>
                        </li>
                        <li className="features__item">
                            <div className="features__inner">
                                <span className="features__icon features__icon--02" />
                                <Translate
                                    content="landing.features.text-02"
                                    className="features__text"
                                />
                            </div>
                        </li>
                        <li className="features__item">
                            <div className="features__inner">
                                <span className="features__icon features__icon--03" />
                                <Translate
                                    content="landing.features.text-03"
                                    className="features__text"
                                />
                            </div>
                        </li>
                        <li className="features__item">
                            <div className="features__inner">
                                <span className="features__icon features__icon--04" />
                                <Translate
                                    content="landing.features.text-04"
                                    className="features__text"
                                />
                            </div>
                        </li>
                        <li className="features__item">
                            <div className="features__inner">
                                <span className="features__icon features__icon--05" />
                                <Translate
                                    content="landing.features.text-05"
                                    className="features__text"
                                />
                            </div>
                        </li>
                        <li className="features__item">
                            <div className="features__inner">
                                <span className="features__icon features__icon--06" />
                                <Translate
                                    content="landing.features.text-06"
                                    className="features__text"
                                />
                            </div>
                        </li>
                        <li className="features__item">
                            <div className="features__inner">
                                <span className="features__icon features__icon--07" />
                                <Translate
                                    content="landing.features.text-07"
                                    className="features__text"
                                />
                            </div>
                        </li>
                    </ul>
                    <div className="features__footer">
                        <Translate
                            content="landing.features.footer-01"
                            className="features__footer-text"
                        />
                        <Translate
                            content="landing.features.footer-02"
                            className="features__footer-text"
                        />
                        <Translate
                            content="landing.features.footer-03"
                            className="features__footer-text"
                        />
                    </div>
                </div>
            </section>
        );
    }
}

export default FeaturesBlock;
