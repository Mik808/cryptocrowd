import React from "react";
import Translate from "react-translate-component";
import {isIOS} from "react-device-detect";
import {Link} from "react-router-dom";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

class SmiBlock extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let smiLogo = [
            {
                img:
                    "https://crowdwiz.biz/ipfs/QmTobGAcSU9GuiaxsSqhDvU1vbDezDsH7yUt3XihuSwFXT/russia24.jpg",
                url:
                    "https://www.kubantv.ru/details/vesti-intervyu-pevets-biznesmen-investor-aleks-malinovskiy/"
            },
            {
                img:
                    "https://crowdwiz.biz/ipfs/QmTobGAcSU9GuiaxsSqhDvU1vbDezDsH7yUt3XihuSwFXT/kuban.png",
                url:
                    "https://kuban24.tv/item/pevets-i-biznesmen-aleks-malinovskii-predstavlyaet-blokchejn-tehnologii"
            },
            {
                img:
                    "https://crowdwiz.biz/ipfs/QmTobGAcSU9GuiaxsSqhDvU1vbDezDsH7yUt3XihuSwFXT/kommersant.jpg",
                url: "https://www.kommersant.ru/doc/4092843"
            },
            {
                img:
                    "https://crowdwiz.biz/ipfs/QmTobGAcSU9GuiaxsSqhDvU1vbDezDsH7yUt3XihuSwFXT/komsomolec.png",
                url:
                    "https://www.mk-kirov.ru/social/2019/08/19/uspeshnye-biznesmeny-i-itspecialisty-nauchat-kirovchan-kak-zarabatyvat-i-stroit-pribylnyy-biznes-na-peredovykh.html"
            },
            {
                img:
                    "https://crowdwiz.biz/ipfs/QmTobGAcSU9GuiaxsSqhDvU1vbDezDsH7yUt3XihuSwFXT/rentv.png",
                url:
                    "http://www.rentv-kirov.ru/news/chto-takoe-blokcheyn-i-pochemu-za-etoy-tekhnologiey-budushchee/"
            },
            {
                img:
                    "https://crowdwiz.biz/ipfs/QmTobGAcSU9GuiaxsSqhDvU1vbDezDsH7yUt3XihuSwFXT/kommersant.jpg",
                url: "https://www.kommersant.ru/doc/3926029"
            },
            {
                img:
                    "https://crowdwiz.biz/ipfs/QmTobGAcSU9GuiaxsSqhDvU1vbDezDsH7yUt3XihuSwFXT/aif.png",
                url:
                    "https://kuban.aif.ru/society/klub_molodyh_millionerov_ekaterina_malina_o_tehnologii_i_budushchem_blokcheyn"
            }
        ];

        let width = window.innerWidth;
        let sliderItems;

        if (width <= 768) {
            sliderItems = 50;
            if (width <= 580) {
                sliderItems = 100;
            }
        } else {
            sliderItems = 33;
        }

        let settings = {
            slidesToShow: 3,
            slidesToScroll: 1,
            autoPlay: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 420,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        };

        return (
            <section className="smi-block">
                <div className="smi-block__wrap index-center-wrap">
                    <Translate
                        content="landing.smi.title"
                        className="smi-block__title block-title"
                        component="div"
                    />
                    <div
                        className={
                            isIOS
                                ? "smi-block__gallery-block ios-controls"
                                : "smi-block__gallery-block"
                        }
                    >
                        <Slider {...settings}>
                            {smiLogo.map((img, index) => (
                                <a
                                    href={img.url}
                                    className="smi-block__gallery-item"
                                    key={index}
                                    target="_blank"
                                >
                                    <img src={img.img} />
                                </a>
                            ))}
                        </Slider>
                    </div>
                </div>
            </section>
        );
    }
}

export default SmiBlock;
