import React from "react";
import Translate from "react-translate-component";
import {Link} from "react-router-dom";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {Carousel} from "react-responsive-carousel";

let infinityLogo = require("assets/icons/infinity-logo.png");

class Infinity extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        let inifityImg = [
            "01.jpg",
            "03.jpg",
            "04.jpg",
            "05.jpg",
            "19.jpg",
            "14.jpg",
            "15.jpg",
            "10.jpg",
            "16.jpg",
            "07.jpg",
            "08.jpg",
            "18.jpg",
            "20.jpg",
            "13.jpg"
        ];

        return (
            <section className="infinity">
                <div className="infinity__wrap index-center-wrap">
                    <Translate
                        content="landing.infinity.title"
                        className="infinity__title block-title"
                        component="div"
                    />
                    <div className="infinity__gallery-block">
                        <div className="infinity__left-column">
                            <img
                                src={infinityLogo}
                                className="infinity__logo"
                                alt="infinity"
                            />
                            <Translate
                                content="landing.infinity.text"
                                className="infinity__text"
                            />
                            <div className="infinity__btn-wrap">
                                <Link
                                    className="infinity__btn index-btn noselect"
                                    to={`/create-account/password`}
                                >
                                    <Translate
                                        content="landing.infinity.btn"
                                        className="index-btn__text"
                                    />
                                </Link>
                            </div>
                        </div>
                        <div className="infinity__right-column">
                            <div className="infinity__gallery-wrap">
                                <Translate
                                    content="landing.infinity.photo-text"
                                    className="infinity__gallery-text"
                                    component="p"
                                />
                                <Carousel
                                    showIndicators={false}
                                    showStatus={false}
                                    infiniteLoop={true}
                                >
                                    {inifityImg.map((img, i) => (
                                        <div key={i}>
                                            <img
                                                src={
                                                    "https://crowdwiz.biz/ipfs/Qma7zwX2XoCREaht1JLnr1k7jxCdGAZ1h6EBXwhGtWore6/infinity_" +
                                                    img
                                                }
                                            />
                                        </div>
                                    ))}
                                </Carousel>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default Infinity;
