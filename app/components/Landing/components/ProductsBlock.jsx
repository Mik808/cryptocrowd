import React from "react";
import Translate from "react-translate-component";
import {Link} from "react-router-dom";
import {isIOS} from "react-device-detect";
import Slider from "react-slick";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

let investment = require("assets/icons/investment_logo.png");
let gamezone = require("assets/icons/gamezone-logo.png");
let matrix = require("assets/icons/matrix-logo.png");
let cwdMarket = require("assets/icons/cwd-market-logo.png");
let video = require("assets/icons/video-logo.png");

class ProductsBlock extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let productsImg = [
            {
                img: investment,
                url: "/investment"
            },
            {
                img: gamezone,
                url: "/gamezone"
            },
            {
                img: matrix,
                url: "/gamezone/matrix-game"
            },
            {
                img: cwdMarket,
                url: "/crowdmarket"
            },
            {
                img: video,
                url: "/create-account/password"
            }
        ];

        let settings = {
            slidesToShow: 3,
            slidesToScroll: 1,
            autoPlay: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 420,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        };

        let currentAccount = this.props.currentAccount;

        return (
            <section className="products">
                <div className="products__wrap index-center-wrap">
                    <Translate
                        content="landing.products.title"
                        className="products__title block-title"
                        component="div"
                    />
                    <div
                        className={
                            isIOS
                                ? "products__gallery-block ios-controls"
                                : "products__gallery-block"
                        }
                    >
                        <Slider {...settings}>
                            {productsImg.map((img, index) => (
                                <Link
                                    to={
                                        currentAccount
                                            ? img.url
                                            : "/create-account/password"
                                    }
                                    className="products__gallery-item"
                                    key={index}
                                >
                                    <img src={img.img} />
                                </Link>
                            ))}
                        </Slider>
                    </div>
                </div>
            </section>
        );
    }
}

export default ProductsBlock;
