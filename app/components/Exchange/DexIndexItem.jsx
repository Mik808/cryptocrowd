import React from "react";
import Translate from "react-translate-component";
import Icon from "../Icon/Icon";
import {Link} from "react-router-dom";
import {Apis} from "bitsharesjs-ws";
import utils from "common/utils";

//STYLES
import "./scss/dex-index.scss";

class DexIndexItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tickerData: {},
            intervalID: 0
        };

        this.getMarketsArray = this.getMarketsArray.bind(this);
    }

    componentDidMount() {
        this.getMarketsArray();

        this.setState({
            intervalID: setInterval(this.getMarketsArray.bind(this), 15000)
        });
    }

    componentWillUnmount() {
        clearInterval(this.state.intervalID);
    }

    getMarketsArray() {
        let base = this.props.asset_data.base;
        let quote = this.props.asset_data.quote;

        Apis.instance()
            .db_api()
            .exec("get_ticker", [base, quote])
            .then(tickerData => {
                this.setState({
                    tickerData: tickerData
                });
            });
    }

    render() {
        let asset_data = this.props.asset_data;
        let isChanged = this.state.tickerData.percent_change;
        let price =
            Math.round(parseFloat(this.state.tickerData.latest) * 100000) /
            100000;
        let base_volume = utils.format_volume(
            parseInt(this.state.tickerData.base_volume)
        );
        let quote = this.state.tickerData.quote;
        let base = this.state.tickerData.base;
        let isOption = this.props.isOption;
        let arrowIcon;
        let itemClass;
        let percentClass;

        if (isChanged > 0) {
            arrowIcon = "dex_arrow_up";
            itemClass = "dex-item dex-item--up";
            percentClass = "dex-data__percent dex-data__percent--up";
        } else {
            arrowIcon = "dex_arrow_down";
            itemClass = "dex-item dex-item--down";
            percentClass = "dex-data__percent dex-data__percent--down";
        }

        return (
            <li className={isChanged != 0 ? itemClass : this.props.itemClass}>
                <Link
                    to={"market/" + asset_data.link}
                    className="dex-item__link"
                >
                    <div
                        className={
                            isOption
                                ? "dex-item__header dex-item__header--option"
                                : "dex-item__header"
                        }
                    >
                        <Icon
                            name={asset_data.icon}
                            className={
                                isOption
                                    ? "dex-item__icon dex-item__icon--option"
                                    : "dex-item__icon"
                            }
                        />
                        <span className="dex-item__name">
                            {asset_data.name}
                        </span>
                    </div>

                    <div className="dex-data__wrap">
                        <div className="dex-data__column">
                            <span className="dex-data__title">
                                1&nbsp;{quote}
                            </span>
                            <div className="dex-data__inner">
                                <span className="dex-data__text">{price}</span>
                                &nbsp;
                                <span className="dex-data__text dex-data__text--asset">
                                    {base}
                                </span>
                            </div>
                        </div>
                        <div className="dex-data__column dex-data__column--percent-wrap">
                            <Translate
                                className="dex-data__title"
                                content="dex_index.percent_change"
                            />

                            <div className="dex-data__inner">
                                <span
                                    className={
                                        isChanged != 0
                                            ? percentClass
                                            : "dex-data__percent"
                                    }
                                >
                                    {this.state.tickerData.percent_change}%
                                </span>
                                &nbsp;
                                {isChanged != 0 ? (
                                    <Icon
                                        name={arrowIcon}
                                        className="dex-data__icon"
                                    />
                                ) : null}
                            </div>
                        </div>
                        <div className="dex-data__column dex-data__column--volume-wrap">
                            <Translate
                                className="dex-data__title"
                                content="dex_index.day_value"
                            />

                            <div className="dex-data__inner">
                                <span className="dex-data__text">
                                    {base_volume}
                                </span>
                                &nbsp;
                                <span className="dex-data__text dex-data__text--asset">
                                    {base}
                                </span>
                            </div>
                        </div>
                    </div>
                </Link>
            </li>
        );
    }
}

export default DexIndexItem;
