import React from "react";
import Translate from "react-translate-component";
import DexIndexItem from "./DexIndexItem";

//STYLES
import "./scss/dex-index.scss";

class DexIndex extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            exchangeData: [
                {
                    base: "1.3.0",
                    quote: "1.3.1",
                    name: "GCWD - CWD",
                    icon: "dex_gcwd_cwd",
                    link: "GCWD_CWD"
                },
                {
                    base: "1.3.0",
                    quote: "1.3.5",
                    name: "mGCWD - CWD",
                    icon: "dex_mgcwd_cwd",
                    link: "MGCWD_CWD"
                },
                {
                    base: "1.3.5",
                    quote: "1.3.1",
                    name: "mGCWD - GCWD",
                    icon: "dex_mgcwd_gcwd",
                    link: "GCWD_MGCWD"
                },
                {
                    base: "1.3.0",
                    quote: "1.3.3",
                    name: "CROWD.BTC - CWD",
                    icon: "dex_btc_cwd",
                    link: "CROWD.BTC_CWD"
                }
            ],
            exchangeDataOption3: [
                {
                    base: "1.3.0",
                    quote: "1.3.7",
                    name: "SILVER.I3700820 - CWD",
                    icon: "dex_option_3",
                    link: "SILVER.I3700820_CWD"
                }
            ],
            exchangeDataOption6: [
                {
                    base: "1.3.0",
                    quote: "1.3.8",
                    name: "SILVER.I61900820 - CWD",
                    icon: "dex_option_6",
                    link: "SILVER.I61900820_CWD"
                }
            ]
        };
    }
    render() {
        let width = window.innerWidth;
        let headerImg;

        if (width > 576) {
            headerImg = require("assets/icons/dex_header_bg_720.png");
        } else if (width > 768) {
            headerImg = require("assets/icons/dex_header_bg.png");
        } else {
            headerImg = require("assets/icons/dex_header_bg_375.png");
        }

        let exchangeData = this.state.exchangeData;
        let exchangeDataOption3 = this.state.exchangeDataOption3;
        let exchangeDataOption6 = this.state.exchangeDataOption6;

        return (
            <section className="cwd-common__wrap">
                <div className="dex-index__wrap">
                    <div className="dex-index__header">
                        <img src={headerImg} alt="DEX" />
                    </div>

                    <div className="dex-index__container">
                        <ul className="dex-index__list">
                            {exchangeData.map((assetObj, index) => (
                                <DexIndexItem
                                    key={index}
                                    itemClass="dex-item"
                                    asset_data={assetObj}
                                    isOption={false}
                                />
                            ))}
                        </ul>

                        {/*OPTION 3*/}
                        <Translate
                            className="dex-index__title"
                            content="dex_index.options_3_title"
                        />
                        <ul className="dex-index__list dex-index__list--option">
                            {exchangeDataOption3.map((options3Obj, index) => (
                                <DexIndexItem
                                    key={index}
                                    itemClass="dex-item dex-item--option"
                                    asset_data={options3Obj}
                                    isOption={true}
                                />
                            ))}
                        </ul>

                        {/*OPTION 6*/}
                        <Translate
                            className="dex-index__title"
                            content="dex_index.options_6_title"
                        />
                        <ul className="dex-index__list dex-index__list--option">
                            {exchangeDataOption6.map((options6Obj, index) => (
                                <DexIndexItem
                                    key={index}
                                    itemClass="dex-item dex-item--option"
                                    asset_data={options6Obj}
                                    isOption={true}
                                />
                            ))}
                        </ul>
                    </div>
                </div>
            </section>
        );
    }
}

export default DexIndex;
