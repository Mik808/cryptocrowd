import React from "react";
import Translate from "react-translate-component";
import {connect} from "alt-react";
import {Tabs, Tab} from "../Utility/Tabs";
import AccountStore from "stores/AccountStore";
import {ChainStore} from "bitsharesjs";
import InvestTransfer from "./components/InvestTransfer";
import Option from "./components/Option";
import {Apis} from "bitsharesjs-ws";

let headerImg = require("assets/icons/invest_bg.png");
let headerImgMobile = require("assets/icons/invest_bg_320.png");

class Investment extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            silverBalance: 0,
            optionBalance_3: 0,
            optionBalance_6: 0,
            currentAccount: null
        };
    }

    componentDidMount() {
        let accountObj = this.props.account;
        if (accountObj) {
            let balance_id = accountObj.getIn(["balances", "1.3.4"]);
            let userID = accountObj.get("id");
            let userName = accountObj.get("name");

            this.setState({
                currentAccount: userName
            });

            Apis.instance()
                .db_api()
                .exec("get_objects", [[balance_id]])
                .then(date => {
                    this.setState({
                        silverBalance: date[0]["balance"] / 100000
                    });
                });

            this.getBalance(userID, "1.3.7", "1.3.8");
        } else {
            this.props.history.push("/create-account/password");
        }
    }

    getBalance(user, asset_3, asset_6) {
        let precision;
        let assetID;

        Apis.instance()
            .db_api()
            .exec("get_assets", [[asset_3]])
            .then(assetObj => {
                precision = assetObj[0]["precision"];
                assetID = assetObj[0]["id"];

                Apis.instance()
                    .db_api()
                    .exec("get_account_balances", [user, [assetID]])
                    .then(accountObj => {
                        this.setState({
                            optionBalance_3:
                                accountObj[0]["amount"] /
                                Math.pow(10, precision)
                        });
                    });
            });

        Apis.instance()
            .db_api()
            .exec("get_assets", [[asset_6]])
            .then(assetObj => {
                precision = assetObj[0]["precision"];
                assetID = assetObj[0]["id"];

                Apis.instance()
                    .db_api()
                    .exec("get_account_balances", [user, [assetID]])
                    .then(accountObj => {
                        this.setState({
                            optionBalance_6:
                                accountObj[0]["amount"] /
                                Math.pow(10, precision)
                        });
                    });
            });
    }

    render() {
        let currentAccount = this.state.currentAccount;
        let percent = {infinity1: "10", infinity3: "50", infinity6: "150"};
        let silverBalance = this.state.silverBalance.toString();
        let optionBalance_3 = this.state.optionBalance_3;
        let optionBalance_6 = this.state.optionBalance_6;
        let width = window.innerWidth;

        return (
            <section className="cwd-common__wrap">
                <div className="invest__header">
                    <img
                        src={width > 767 ? headerImg : headerImgMobile}
                        alt="investment"
                    />
                </div>

                <div className="invest__intro">
                    <Translate
                        className="cwd-common__title"
                        content="invest.title"
                    />
                    <Translate
                        className="cwd-common__description invest__intro-text"
                        content="invest.description"
                        component="p"
                    />
                    <Translate
                        className="cwd-common__description"
                        content="invest.description_02"
                        component="p"
                    />
                </div>

                <Tabs
                    className="cwd-tabs"
                    tabsClass="cwd-tabs__list"
                    contentClass="cwd-tabs__content"
                    segmented={false}
                    actionButtons={false}
                >
                    <Tab title="invest.tab_01">
                        <InvestTransfer
                            key="infinity1"
                            currentAccount={currentAccount}
                            toName="infinity1"
                            percent={percent["infinity1"]}
                            minDeposit="200"
                            transferAsset="1.3.0"
                        />
                    </Tab>
                    <Tab title="invest.tab_02">
                        <div className="invest-tab__wrap">
                            {/* <Translate
                                className="cwd-common__description cwd-common__description--highlighted"
                                content="invest.promo_peecent"
                                component="p"
                            /> */}
                            <InvestTransfer
                                key="infinity3"
                                currentAccount={currentAccount}
                                toName="infinity3"
                                percent={percent["infinity3"]}
                                minDeposit="250"
                                transferAsset="1.3.0"
                            />

                            {optionBalance_3 > 0 ? (
                                <Option
                                    key="investOption1"
                                    currentAccount={currentAccount}
                                    toName="infinity-i3700820"
                                    minDeposit="1"
                                    transferAsset="1.3.7"
                                />
                            ) : (
                                <section className="invest__transfer-wrap invest__transfer-wrap--option">
                                    <Translate
                                        className="cwd-common__subtitle"
                                        content="invest.crowd_options"
                                    />
                                    <Translate
                                        className="cwd-common__description"
                                        content="invest.non_options"
                                    />
                                </section>
                            )}
                        </div>
                    </Tab>
                    <Tab title="invest.tab_03">
                        <div className="invest-tab__wrap">
                            <InvestTransfer
                                key="infinity6"
                                currentAccount={currentAccount}
                                toName="infinity6"
                                percent={percent["infinity6"]}
                                minDeposit="250"
                                transferAsset="1.3.0"
                            />

                            {optionBalance_6 > 0 ? (
                                <Option
                                    key="investOption1"
                                    currentAccount={currentAccount}
                                    toName="infinity-i61900820"
                                    minDeposit="1"
                                    transferAsset="1.3.8"
                                />
                            ) : (
                                <section className="invest__transfer-wrap invest__transfer-wrap--option">
                                    <Translate
                                        className="cwd-common__subtitle"
                                        content="invest.crowd_options"
                                    />
                                    <Translate
                                        className="cwd-common__description"
                                        content="invest.non_options"
                                    />
                                </section>
                            )}
                        </div>
                    </Tab>
                    <Tab title="invest.tab_04">
                        <InvestTransfer
                            key="infinity4"
                            currentAccount={currentAccount}
                            toName="infinity-withdraw"
                            percent="0"
                            minDeposit={silverBalance}
                            transferAsset="1.3.4"
                        />
                    </Tab>
                </Tabs>
            </section>
        );
    }
}

export default Investment = connect(Investment, {
    listenTo() {
        return [AccountStore];
    },
    getProps() {
        if (AccountStore.getState().passwordAccount) {
            return {
                account: ChainStore.fetchFullAccount(
                    AccountStore.getState().passwordAccount
                )
            };
        }
    }
});
