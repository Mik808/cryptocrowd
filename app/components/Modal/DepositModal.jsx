import React from "react";
import {DecimalChecker} from "../Utility/DecimalChecker";
import {
    _getNumberAvailableGateways,
    _onAssetSelected,
    _getCoinToGatewayMapping
} from "lib/common/assetGatewayMixin";
import counterpart from "counterpart";
import {Modal, Button} from "crowdwiz-ui-modal";
import Translate from "react-translate-component";
import {Link} from "react-router-dom";

class DepositModalContent extends DecimalChecker {
    constructor(props) {
        super(props);
        this.state = {
            account_name: this.props.account,
            BTCdata: []
        };
    }

    UNSAFE_componentWillMount() {
        let accountName = this.state.account_name;

        const xhr = new XMLHttpRequest();

        xhr.open(
            "GET",
            "https://api.crowdwiz.biz/deposit?login=" +
                accountName +
                "&ticker=btc",
            true
        );

        xhr.send();

        xhr.onreadystatechange = () => {
            if (xhr.status == 200) {
                console.log(xhr.responseText);
                this.setState({
                    BTCdata: JSON.parse(xhr.responseText)
                });
            } else {
                console.log("Error");
            }
        };
    }

    selectValue(id, e) {
        document.getElementById(id).select();
    }

    render() {
        let BTCdata = this.state.BTCdata;

        return (
            <div className="grid-block vertical">
                <div className="gateway__wrap">
                    <Translate
                        className="gateway__text"
                        content="gatewayBTC.marketText1"
                    />

                    <div className="gateway__rate">
                        <Translate content="gatewayBTC.rate" />
                        <span className="gateway__text">
                            1 CROWD.BTC = 1 BTC
                        </span>
                    </div>

                    <div className="gateway__item">
                        <Translate
                            content="gatewayBTC.title"
                            className="gateway__text gateway__text--addr"
                        />
                        <input
                            id="BTCAddress"
                            className="gateway__wallet"
                            defaultValue={BTCdata["addr"]}
                            onClick={this.selectValue.bind(this, "BTCAddress")}
                        />
                    </div>

                    <Translate
                        className="gateway__text"
                        content="gatewayBTC.marketText2"
                    />
                    <Link
                        className="gateway__text"
                        to={`/market/CROWD.BTC_CWD`}
                    >
                        <Translate content="gatewayBTC.marketText3" />
                    </Link>

                    <Translate
                        className="gateway__warning"
                        content="gatewayBTC.warning"
                    />
                </div>
            </div>
        );
    }
}

export default class DepositModal extends React.Component {
    constructor() {
        super();

        this.state = {open: false};
    }

    show() {
        this.setState({open: true}, () => {
            this.props.hideModal();
        });
    }

    onClose() {
        this.props.hideModal();
        this.setState({open: false});
    }

    render() {
        return (
            <Modal
                className="cwd-deposit-modal"
                title={
                    this.props.account
                        ? counterpart.translate("modal.deposit.header", {
                              account_name: this.props.account
                          })
                        : counterpart.translate("modal.deposit.header_short")
                }
                id={this.props.modalId}
                onCancel={this.onClose.bind(this)}
                overlay={true}
                footer={[
                    <Button key="cancel" onClick={this.props.hideModal}>
                        {counterpart.translate("modal.close")}
                    </Button>
                ]}
                visible={this.props.visible}
                noCloseBtn
            >
                <DepositModalContent
                    hideModal={this.props.hideModal}
                    {...this.props}
                    open={this.props.visible}
                />
            </Modal>
        );
    }
}
