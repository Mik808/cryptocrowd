import React from "react";
import {connect} from "alt-react";
import Icon from "../Icon/Icon";
import AccountStore from "stores/AccountStore";
import SettingsActions from "actions/SettingsActions";
import Translate from "react-translate-component";
import Footer from "../Layout/Footer";
import {Apis} from "bitsharesjs-ws";
import {ChainStore} from "bitsharesjs";
import WalletUnlockStore from "stores/WalletUnlockStore";
import DepositModal from "../Modal/DepositModal";
import WithdrawModal from "../Modal/WithdrawModalNew";
import SendModal from "../Modal/SendModal";
import SettingsStore from "stores/SettingsStore";

import "./scss/left-menu.scss";

class LeftMenu extends React.Component {
    constructor() {
        super();

        this.state = {
            toggle: false,
            finance: false,
            isDepositModalVisible: false,
            hasDepositModalBeenShown: false,
            isWithdrawModalVisible: false,
            hasWithdrawalModalBeenShown: false,
            synced: this._syncStatus(),
            toggleMenu: false
        };

        this._syncStatus = this._syncStatus.bind(this);
    }

    componentDidMount() {
        this.syncCheckInterval = setInterval(
            this._syncStatus.bind(this, true),
            5000
        );
    }

    componentWillUnmount() {
        clearInterval(this.syncCheckInterval);
    }

    toggleButton = () => {
        this.setState({
            toggle: !this.state.toggle,
            finance: false,
            toggleMenu: false
        });
    };

    toggleOff = () => {
        this.setState({
            finance: false,
            toggleMenu: false,
            toggle: false
        });
    };

    toggleFinance = () => {
        this.setState({
            finance: !this.state.finance,
            toggleMenu: false
        });
    };

    toggleOffFinance = () => {
        this.setState({
            finance: true,
            toggle: true
        });
    };

    toggleSubmenuOn = () => {
        this.setState({
            toggleMenu: !this.state.toggleMenu,
            finance: false
        });
    };

    toggleSubmenuOff = () => {
        this.setState({
            toggleMenu: true,
            toggle: true
        });
    };

    _onNavigate(route, e) {
        e.preventDefault();

        // Set Accounts Tab as active tab
        if (route == "/accounts") {
            SettingsActions.changeViewSetting({
                dashboardEntry: "accounts"
            });
        }

        this.props.history.push(route);
        this.toggleOff();
    }

    _showSend = e => {
        e.preventDefault();
        if (this.send_modal) this.send_modal.show();
    };

    showWithdrawModal = () => {
        this.setState({
            isWithdrawModalVisible: true,
            hasWithdrawalModalBeenShown: true
        });
    };

    hideWithdrawModal = () => {
        this.setState({
            isWithdrawModalVisible: false
        });
    };

    showDepositModal = () => {
        this.setState({
            isDepositModalVisible: true,
            hasDepositModalBeenShown: true
        });
    };

    hideDepositModal = () => {
        this.setState({
            isDepositModalVisible: false
        });
    };

    _syncStatus(setState = false) {
        let synced = this.getBlockTimeDelta() < 5;
        if (setState && synced !== this.state.synced) {
            this.setState({synced});
        }
        return synced;
    }

    getBlockTimeDelta() {
        try {
            let bt =
                (this.getBlockTime().getTime() +
                    ChainStore.getEstimatedChainTimeOffset()) /
                1000;
            let now = new Date().getTime() / 1000;
            return Math.abs(now - bt);
        } catch (err) {
            return -1;
        }
    }

    render() {
        const {
            toggle,
            finance,
            toggleMenu,
            hasDepositModalBeenShown,
            hasWithdrawalModalBeenShown,
            isDepositModalVisible,
            isWithdrawModalVisible
        } = this.state;

        let style = "leftmenu__btn--toggleOff";

        if (toggle) {
            style = "leftmenu__btn--toggleOn";
        }

        {
            /*FINANCE MENU BLOCK*/
        }

        const financeSubMenu = finance
            ? [
                  {
                      mainText: "header.payments",
                      mainCallback: this._showSend,
                      subText: "header.payments_legacy"
                  },
                  {
                      mainText: "header.deposit_withdraw",
                      mainCallback: this._onNavigate.bind(this, "/gateway")
                  },
                  {
                      mainText: "header.invest",
                      mainCallback: this._onNavigate.bind(this, "/investment")
                  },
                  {
                      mainText: "account.portfolio_menu",
                      mainCallback: this._onNavigate.bind(
                          this,
                          `/account/${this.props.currentAccount}/portfolio`
                      )
                  }
              ].map(({mainText, mainCallback}, index) => (
                  <li
                      key={index}
                      className="leftmenu__subtext"
                      onClick={mainCallback}
                  >
                      <div>
                          <Translate content={mainText} />
                      </div>
                  </li>
              ))
            : null;

        const financeMenuToggleOn = !this.props.locked ? (
            <li className="leftmenu__li">
                <div>
                    <Icon size="1_5x" name="finance" title="header.finance" />
                </div>
                <div onClick={this.toggleFinance}>
                    <Translate
                        className="leftmenu__text"
                        content="header.finance"
                    />
                </div>
                <ul className="leftmenu__submenu">{financeSubMenu}</ul>
            </li>
        ) : null;

        const financeMenuToggleOff = !this.props.locked ? (
            <li className="leftmenu__li" onClick={this.toggleOffFinance}>
                <div>
                    <Icon size="1_5x" name="finance" title="header.finance" />
                </div>
            </li>
        ) : null;

        const toggleOff = (
            <div className="leftmenu__container--toggleOff">
                <ul className="leftmenu__ul">
                    <li
                        className="leftmenu__li"
                        onClick={this._onNavigate.bind(this, "/gamezone")}
                    >
                        <div>
                            <Icon
                                size="1_5x"
                                name="gamepad"
                                title="account.gamezone.gamezone_header"
                            />
                        </div>
                    </li>

                    <li
                        className="leftmenu__li"
                        onClick={this._onNavigate.bind(this, "/crowdmarket")}
                    >
                        <div>
                            <Icon
                                size="1_5x"
                                name="crowdmarket-icon"
                                title="crowdmarket.crowdmarket_header"
                            />
                        </div>
                    </li>

                    {financeMenuToggleOff}

                    <li
                        className="leftmenu__li"
                        onClick={this._onNavigate.bind(
                            this,
                            "/markets-dashboard"
                        )}
                    >
                        <div>
                            <Icon
                                size="1_5x"
                                name="exchange"
                                title="header.exchange"
                            />
                        </div>
                    </li>

                    {this.props.currentAccount ? (
                        <li
                            className="leftmenu__li"
                            onClick={this._onNavigate.bind(
                                this,
                                `/account/${this.props.currentAccount}/vesting`
                            )}
                        >
                            <div>
                                <Icon
                                    size="1_5x"
                                    name="vesting-desc"
                                    title="header.my_status"
                                />
                            </div>
                        </li>
                    ) : null}
                    {this.props.currentAccount ? (
                        <li
                            className="leftmenu__li"
                            onClick={this._onNavigate.bind(
                                this,
                                `/account/${this.props.currentAccount}/structure`
                            )}
                        >
                            <div>
                                <Icon
                                    size="1_5x"
                                    name="activity"
                                    title="header.structure"
                                />
                            </div>
                        </li>
                    ) : null}
                    {this.props.currentAccount ? (
                        <li
                            className="leftmenu__li"
                            onClick={this._onNavigate.bind(
                                this,
                                `/account/${this.props.currentAccount}/voting`
                            )}
                        >
                            <div>
                                <Icon
                                    size="1_5x"
                                    name="vote"
                                    title="account.voting"
                                />
                            </div>
                        </li>
                    ) : null}

                    {this.props.currentAccount ? (
                        <li
                            className="leftmenu__li"
                            onClick={this._onNavigate.bind(
                                this,
                                `/account/${this.props.currentAccount}`
                            )}
                        >
                            <div>
                                <Icon
                                    size="1_5x"
                                    name="arrow_4"
                                    title="account.activity"
                                />
                            </div>
                        </li>
                    ) : null}

                    <li
                        className="leftmenu__li"
                        onClick={this._onNavigate.bind(
                            this,
                            "/explorer/blocks"
                        )}
                    >
                        <div>
                            <Icon
                                size="1_5x"
                                name="eye"
                                title="account.explorer"
                            />
                        </div>
                    </li>
                </ul>

                <div className="leftmenu__indicator">
                    <Footer
                        synced={this.state.synced}
                        history={this.props.history}
                        toggleFooter={false}
                    />
                </div>
            </div>
        );

        const toggleOn = (
            <div className="leftmenu__container">
                <ul className="leftmenu__ul">
                    <li
                        className="leftmenu__li"
                        onClick={this._onNavigate.bind(this, "/gamezone")}
                    >
                        <div>
                            <Icon
                                size="1_5x"
                                name="gamepad"
                                title="account.gamezone.gamezone_header"
                            />
                        </div>
                        <div>
                            <Translate
                                className="leftmenu__text"
                                content="account.gamezone.gamezone_header"
                            />
                        </div>
                    </li>

                    <li
                        className="leftmenu__li"
                        onClick={this._onNavigate.bind(this, "/crowdmarket")}
                    >
                        <div>
                            <Icon
                                size="1_5x"
                                name="crowdmarket-icon"
                                title="crowdmarket.crowdmarket_header"
                            />
                        </div>
                        <div>
                            <Translate
                                className="leftmenu__text"
                                content="crowdmarket.crowdmarket_header"
                            />
                        </div>
                    </li>

                    {financeMenuToggleOn}

                    <li
                        className="leftmenu__li"
                        onClick={this._onNavigate.bind(
                            this,
                            "/markets-dashboard"
                        )}
                    >
                        <div>
                            <Icon
                                size="1_5x"
                                name="exchange"
                                title="header.exchange"
                            />
                        </div>
                        <div>
                            <Translate
                                className="leftmenu__text"
                                content="header.exchange"
                            />
                        </div>
                    </li>

                    {this.props.currentAccount ? (
                        <li
                            className="leftmenu__li"
                            onClick={this._onNavigate.bind(
                                this,
                                `/account/${this.props.currentAccount}/vesting`
                            )}
                        >
                            <div>
                                <Icon
                                    size="1_5x"
                                    name="vesting-desc"
                                    title="header.my_status"
                                />
                            </div>
                            <div>
                                <Translate
                                    className="leftmenu__text"
                                    content="header.my_status"
                                />
                            </div>
                        </li>
                    ) : null}

                    {this.props.currentAccount ? (
                        <li
                            className="leftmenu__li"
                            onClick={this._onNavigate.bind(
                                this,
                                `/account/${this.props.currentAccount}/structure`
                            )}
                        >
                            <div>
                                <Icon
                                    size="1_5x"
                                    name="activity"
                                    title="header.structure"
                                />
                            </div>
                            <div>
                                <Translate
                                    className="leftmenu__text"
                                    content="header.structure"
                                />
                            </div>
                        </li>
                    ) : null}

                    {this.props.currentAccount ? (
                        <li
                            className="leftmenu__li"
                            onClick={this._onNavigate.bind(
                                this,
                                `/account/${this.props.currentAccount}/voting`
                            )}
                        >
                            <div>
                                <Icon
                                    size="1_5x"
                                    name="vote"
                                    title="account.voting"
                                />
                            </div>
                            <div>
                                <Translate
                                    className="leftmenu__text"
                                    content="account.voting"
                                />
                            </div>
                        </li>
                    ) : null}

                    {this.props.currentAccount ? (
                        <li
                            className="leftmenu__li"
                            onClick={this._onNavigate.bind(
                                this,
                                `/account/${this.props.currentAccount}`
                            )}
                        >
                            <div>
                                <Icon
                                    size="1_5x"
                                    name="arrow_4"
                                    title="account.activity"
                                />
                            </div>
                            <div>
                                <Translate
                                    className="leftmenu__text"
                                    content="account.activity"
                                />
                            </div>
                        </li>
                    ) : null}

                    <li
                        className="leftmenu__li"
                        onClick={this._onNavigate.bind(
                            this,
                            "/explorer/blocks"
                        )}
                    >
                        <div>
                            <Icon
                                size="1_5x"
                                name="eye"
                                title="account.explorer"
                            />
                        </div>
                        <div>
                            <Translate
                                className="leftmenu__text"
                                content="account.explorer"
                            />
                        </div>
                    </li>
                </ul>

                <div className="leftmenu__footer-wrap">
                    <Footer
                        synced={this.state.synced}
                        history={this.props.history}
                        toggleFooter={true}
                    />
                </div>
            </div>
        );

        return (
            <div className="leftmenu">
                <div
                    onClick={this.toggleButton}
                    className="leftmenu__btn-wrapper"
                >
                    <div className={style} />
                </div>

                {toggle && toggleOn}
                {!toggle && toggleOff}

                <SendModal
                    id="send_modal_header"
                    refCallback={e => {
                        if (e) this.send_modal = e;
                    }}
                    from_name={this.props.currentAccount}
                />
                {hasDepositModalBeenShown && (
                    <DepositModal
                        visible={isDepositModalVisible}
                        hideModal={this.hideDepositModal}
                        showModal={this.showDepositModal}
                        ref="deposit_modal_new"
                        modalId="deposit_modal_new"
                        account={this.props.currentAccount}
                    />
                )}
                {hasWithdrawalModalBeenShown && (
                    <WithdrawModal
                        visible={isWithdrawModalVisible}
                        hideModal={this.hideWithdrawModal}
                        showModal={this.showWithdrawModal}
                        ref="withdraw_modal_new"
                        modalId="withdraw_modal_new"
                    />
                )}
            </div>
        );
    }
}

LeftMenu = connect(LeftMenu, {
    listenTo() {
        return [AccountStore, WalletUnlockStore, SettingsStore];
    },
    getProps() {
        const chainID = Apis.instance().chain_id;
        return {
            myActiveAccounts: AccountStore.getState().myActiveAccounts,
            currentAccount:
                AccountStore.getState().currentAccount ||
                AccountStore.getState().passwordAccount,
            passwordAccount: AccountStore.getState().passwordAccount,
            locked: WalletUnlockStore.getState().locked,
            lastMarket: SettingsStore.getState().viewSettings.get(
                `lastMarket${chainID ? "_" + chainID.substr(0, 8) : ""}`
            )
        };
    }
});

export default LeftMenu;
