import React from "react";
import Translate from "react-translate-component";
import WalletUnlockActions from "actions/WalletUnlockActions";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import WalletDb from "stores/WalletDb";
import {ChainStore} from "bitsharesjs";
import {Tabs, Tab} from "../Utility/Tabs";
import counterpart from "counterpart";
import {connect} from "alt-react";
import PostAdModal from "../Modal/CrowdMarketPayModal";
import BuyAdModal from "../Modal/CrowdMarketBuyAdModal";
import Icon from "../Icon/Icon";
import AdsForm from "./AdsForm";
import ScrollTo from "react-scroll-into-view";

class CrowdAds extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ads: [],
            myAds: [],
            currentAccount: AccountStore.getState().currentAccount,
            userData: {},
            isPostAdModalVisible: false,
            hasPostAdModalBeenShown: false,
            isBuyItemModalVisible: false,
            hasBuyItemModalBeenShown: false,
            adId: 0,
            adTitle: "",
            showMore: false,
            openedCard: null,
            amount: null,
            fromUserId: "",
            adBuyTitle: "",
            adBuyId: "",
            currentCat: null,
            menuItemMob: "categories__item--hide",
            intervalID: 0
        };
        this.getAdsArray = this.getAdsArray.bind(this);
        this.getMyAdsArray = this.getMyAdsArray.bind(this);
    }

    componentDidMount() {
        this.getMyAdsArray();
        this.getAdsArray("cat01");
        this.getStructure();

        this.setState({
            intervalID: setInterval(this.getMyAdsArray.bind(this), 5000)
        });
    }

    componentWillUnmount() {
        clearInterval(this.state.intervalID);
    }

    getStructure() {
        if (this.state.currentAccount) {
            let userID = this.props.account.get("id");
            var url =
                "https://crowdwiz.biz/structure/cache/" + userID + "-p.json";

            fetch(url)
                .then(response => response.json())
                .then(data => {
                    this.setState({
                        userData: data
                    });
                });
        }
    }

    getAdsArray(catId) {
        this.setState({currentCat: catId});

        this.setState({
            ads: []
        });

        var url =
            "https://crowdwiz.biz/board_add/api/get_active_items/" + catId;

        fetch(url)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    ads: data
                });
            })
            .catch(err => {
                console.log("error:", err);
            });

        if (catId) {
            var elems = document.getElementsByClassName("categories__item");
            for (var i = 0; i < elems.length; i++) {
                elems[i].classList.remove("categories__item--active");
            }

            document
                .getElementById(catId)
                .classList.add("categories__item--active");

            document.getElementById(
                "boardContent"
            ).scrollTop = document.getElementById("boardList").offsetTop;
        }
    }

    getMyAdsArray() {
        if (this.state.currentAccount) {
            var url =
                "https://crowdwiz.biz/board_add/api/get_users_items/" +
                this.state.currentAccount;

            fetch(url)
                .then(response => response.json())
                .then(data => {
                    this.setState({
                        myAds: data
                    });
                })
                .catch(err => {
                    console.log("error:", err);
                });
        }
    }

    showBuyItemModal(amount, name, ad_buy_title, ad_buy_id) {
        if (this.props.account) {
            if (WalletDb.isLocked()) {
                WalletUnlockActions.unlock()
                    .then(() => {
                        AccountActions.tryToSetCurrentAccount();
                        this.setState({
                            isBuyItemModalVisible: true,
                            hasBuyItemModalBeenShown: true,
                            amount: amount,
                            fromUserId: name,
                            adBuyTitle: ad_buy_title,
                            adBuyId: ad_buy_id
                        });
                    })
                    .catch(() => {});
            } else {
                this.setState({
                    isBuyItemModalVisible: true,
                    hasBuyItemModalBeenShown: true,
                    amount: amount,
                    fromUserId: name,
                    adBuyTitle: ad_buy_title,
                    adBuyId: ad_buy_id
                });
            }
        } else {
            this.props.history.push("/create-account/password");
        }
    }

    hideBuyItemModal = () => {
        this.setState({
            isBuyItemModalVisible: false
        });
    };

    showPostAdModal = (adId, adTitle) => {
        this.setState({
            isPostAdModalVisible: true,
            hasPostAdModalBeenShown: true,
            adId: adId,
            adTitle: adTitle
        });
    };

    hidePostAdModal = () => {
        this.setState({
            isPostAdModalVisible: false
        });
    };

    showMore(adId) {
        this.setState({showMore: !this.state.showMore});

        if (this.state.openedCard === adId) {
            this.setState({openedCard: null});
        } else {
            this.setState({openedCard: adId});
        }
    }

    toggleMenu() {
        if (this.state.menuItemMob === "categories__item--show") {
            this.setState({menuItemMob: "categories__item--hide"});
        } else {
            this.setState({menuItemMob: "categories__item--show"});
        }
    }

    render() {
        let ads = this.state.ads;
        let myAds = this.state.myAds;

        let showNewAd = false;
        if (this.state.userData["status"] === 4) {
            showNewAd = true;
        }

        let cats = {
            cat01: counterpart.translate("crowdmarket.cat-01"),
            cat02: counterpart.translate("crowdmarket.cat-02"),
            cat03: counterpart.translate("crowdmarket.cat-03"),
            cat04: counterpart.translate("crowdmarket.cat-04"),
            cat05: counterpart.translate("crowdmarket.cat-05"),
            cat06: counterpart.translate("crowdmarket.cat-06"),
            cat07: counterpart.translate("crowdmarket.cat-07"),
            cat08: counterpart.translate("crowdmarket.cat-08"),
            cat09: counterpart.translate("crowdmarket.cat-09"),
            cat10: counterpart.translate("crowdmarket.cat-10")
        };

        let statuses = [
            counterpart.translate("crowdmarket.status-0"),
            counterpart.translate("crowdmarket.status-01"),
            counterpart.translate("crowdmarket.status-02")
        ];

        let {
            isPostAdModalVisible,
            isBuyItemModalVisible,
            adId,
            adTitle,
            amount,
            fromUserId,
            adBuyTitle,
            adBuyId
        } = this.state;

        return (
            <div className="crowd-board__wrap">
                <div className="crowd-board__content" id="boardContent">
                    <Tabs
                        className="cwd-tabs"
                        tabsClass="cwd-tabs__list"
                        contentClass="cwd-tabs__content"
                        segmented={false}
                        actionButtons={false}
                    >
                        {/* ACTIVE ADS */}
                        <Tab title="crowdmarket.active-ads">
                            {/* CATEGORIES */}
                            <ul
                                className="categories mdl-button mdl-js-button mdl-button--raised"
                                selector="#boardList"
                            >
                                {/* <li
                                    className={
                                        this.state.menuItemMob ===
                                        "categories__item--show"
                                            ? "categories__close categories__close--show"
                                            : "categories__close"
                                    }
                                    onClick={() => this.toggleMenu()}
                                >
                                    <Icon size="1x" name="hamburger-x" />
                                </li> */}
                                {Object.entries(cats).map(
                                    ([category, category_name], index) => (
                                        <li
                                            className={
                                                index > 2
                                                    ? `categories__item ${this.state.menuItemMob}`
                                                    : "categories__item"
                                            }
                                            key={category}
                                            id={category}
                                            onClick={this.getAdsArray.bind(
                                                this,
                                                category
                                            )}
                                        >
                                            <ScrollTo
                                                selector="#boardList"
                                                smooth={true}
                                            >
                                                <span className="categories__text">
                                                    {category_name}
                                                </span>

                                                {this.state.ads.length &&
                                                this.state.currentCat ===
                                                    category ? (
                                                    <span className="categories__amount">
                                                        {this.state.ads.length}
                                                    </span>
                                                ) : null}
                                            </ScrollTo>
                                        </li>
                                    )
                                )}
                                <li
                                    className="categories__item categories__show-all"
                                    onClick={() => this.toggleMenu()}
                                >
                                    <Icon size="1x" name="hamburger" />
                                    <Translate content="crowdmarket.all-categories" />
                                </li>
                            </ul>

                            {/* BOARD ARRAY */}
                            <ul className="board" id="boardList">
                                {ads.map(ad => (
                                    <li
                                        className={`board__item ${
                                            this.state.openedCard === ad["id"]
                                                ? "board__item--more"
                                                : ""
                                        } `}
                                        key={ad["id"]}
                                    >
                                        <div
                                            className={`board__wrap  ${
                                                this.state.openedCard ===
                                                ad["id"]
                                                    ? "board__wrap--more"
                                                    : ""
                                            } `}
                                        >
                                            <div className="board__img">
                                                <img
                                                    src={ad["ipfs_url"]}
                                                    alt=""
                                                />
                                                <span className="board__subtitle">
                                                    {ad["id"]}
                                                </span>
                                            </div>
                                            <div className="board__center">
                                                <span className="board__name">
                                                    {ad["title"]}
                                                </span>

                                                <p className="board__description">
                                                    {ad["description"]}
                                                </p>
                                            </div>
                                        </div>
                                        <div
                                            className="board__more-wrapper"
                                            onClick={() =>
                                                this.showMore(ad["id"])
                                            }
                                        >
                                            <div
                                                className={`board__more-btn ${
                                                    this.state.openedCard ===
                                                    ad["id"]
                                                        ? "board__more-btn--opened"
                                                        : ""
                                                } `}
                                            />
                                        </div>
                                        <div className="board__details">
                                            <div className="board__price-wrap">
                                                <span className="board__price">
                                                    {ad["price"]} CWD
                                                </span>
                                            </div>
                                            <div className="board__data-text-wrap">
                                                <span className="board__data-text">
                                                    {ad["contacts"]}
                                                </span>
                                            </div>
                                            <span
                                                className="board__buy-btn"
                                                onClick={this.showBuyItemModal.bind(
                                                    this,
                                                    ad["price"],
                                                    ad["name"],
                                                    ad["title"],
                                                    ad["id"]
                                                )}
                                            >
                                                <Translate content="crowdmarket.buy-item" />
                                            </span>
                                        </div>
                                    </li>
                                ))}
                            </ul>
                        </Tab>

                        {/*  MY ADS */}
                        {this.props.account ? (
                            <Tab
                                title="crowdmarket.my-ads"
                                onClick={this.getMyAdsArray}
                            >
                                <ul className="board">
                                    {myAds.map(myAd => (
                                        <li
                                            className="board__item board__item--my-board"
                                            key={myAd["id"]}
                                            id={myAd["id"]}
                                        >
                                            <div className="board__img board__img--my-board">
                                                <span className="board__subtitle">
                                                    {myAd["id"]}
                                                </span>
                                                <img
                                                    src={myAd["ipfs_url"]}
                                                    alt=""
                                                />
                                            </div>
                                            <div className="board__center board__center--my-board">
                                                <span className="board__name">
                                                    {myAd["title"]}
                                                </span>
                                                <div className="board__data-text-wrap board__data-text-wrap--status">
                                                    <Translate
                                                        component="span"
                                                        className="board__data-text board__data-text--text"
                                                        content="crowdmarket.board-status"
                                                    />
                                                    <span className="board__data-text">
                                                        {
                                                            statuses[
                                                                myAd[
                                                                    "item_status"
                                                                ]
                                                            ]
                                                        }
                                                    </span>
                                                </div>
                                            </div>
                                            {myAd["item_status"] == 1 ? (
                                                <div className="board__btn-wrap">
                                                    <button
                                                        className="board__buy-btn board__buy-btn--contacts"
                                                        onClick={this.showPostAdModal.bind(
                                                            this,
                                                            myAd["id"],
                                                            myAd["title"]
                                                        )}
                                                    >
                                                        <Translate content="crowdmarket.pay-new-ad" />
                                                    </button>
                                                </div>
                                            ) : null}
                                            {/*{myAd['item_status'] ==2 ?
                                                <div className="board__btn-wrap">
                                                    <button
                                                        className="board__buy-btn board__buy-btn--contacts board__buy-btn--del"
                                                    >
                                                        <Translate content="crowdmarket.delete-ad" />
                                                    </button>
                                                </div>
                                    : null}*/}
                                        </li>
                                    ))}
                                </ul>
                            </Tab>
                        ) : null}

                        {/* NEW AD FORM */}
                        {showNewAd ? (
                            <Tab title="crowdmarket.new-ad">
                                <AdsForm />
                            </Tab>
                        ) : null}
                    </Tabs>

                    {/* MODAL BLOCK */}
                    <BuyAdModal
                        visible={isBuyItemModalVisible}
                        hideModal={this.hideBuyItemModal}
                        showModal={this.showBuyItemModal}
                        ref="buy_ad_item_modal"
                        modalId="buy_ad_item_modal"
                        amount={amount}
                        toName={fromUserId}
                        adBuyTitle={adBuyTitle}
                        adBuyId={adBuyId}
                    />

                    <PostAdModal
                        visible={isPostAdModalVisible}
                        hideModal={this.hidePostAdModal}
                        showModal={this.showPostAdModal}
                        ref="post-ad_modal"
                        modalId="post-ad_modal"
                        adId={adId}
                        adTitle={adTitle}
                    />
                    {/*END MODAL BLOCK */}
                </div>
            </div>
        );
    }
}
export default CrowdAds = connect(CrowdAds, {
    listenTo() {
        return [AccountStore];
    },
    getProps() {
        if (AccountStore.getState().passwordAccount) {
            return {
                account: ChainStore.fetchFullAccount(
                    AccountStore.getState().passwordAccount
                ),
                currentAccount: AccountStore.getState().currentAccount
            };
        }
    }
});
