import React from "react";
import Translate from "react-translate-component";
import {Link} from "react-router-dom";
import Lottie from "react-lottie";
import * as animationData from "./crowdMarketTitle.json";

class CrowdMarketTitle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {isStopped: false, isPaused: false};
    }

    render() {
        const defaultOptions = {
            loop: false,
            autoplay: true,
            animationData: animationData.default,
            rendererSettings: {
                preserveAspectRatio: "xMidYMid slice"
            }
        };

        return (
            <div className="crowdmarket__title-wrapper">
                <Lottie options={defaultOptions} />
            </div>
        );
    }
}

class CrowdMarket extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="crowdmarket">
                <div className="crowdmarket__center-layout">
                    <div className="crowdmarket__header-wrap">
                        <CrowdMarketTitle />
                    </div>

                    <Translate
                        className="crowdmarket__subtitle"
                        content="crowdmarket.subtitle"
                        component="h1"
                    />

                    <ul className="cmarket__list">
                        <li className="cmarket__item">
                            <div className="cmarket__content cmarket__content--board">
                                <div className="cmarket__inner">
                                    <Translate
                                        className="cmarket__title"
                                        content="crowdmarket.crowd-ads"
                                        component="p"
                                    />

                                    <Link
                                        className="cmarket__btn noselect"
                                        to={`crowdmarket/ads-board`}
                                    >
                                        <Translate
                                            className="cmarket__btn-text"
                                            content="crowdmarket.buy-btn"
                                            component="span"
                                        />
                                    </Link>
                                </div>
                            </div>
                        </li>

                        <li className="cmarket__item">
                            <Translate
                                className="cmarket__label"
                                content="gamezone.soon"
                                component="span"
                            />
                            <div className="cmarket__content cmarket__content--inactive cmarket__content--crowd-shopping">
                                <div className="cmarket__inner">
                                    <Translate
                                        className="cmarket__title"
                                        content="crowdmarket.crowd-benefit"
                                        component="p"
                                    />

                                    <Link
                                        className="cmarket__btn noselect"
                                        to={`crowdmarket`}
                                    >
                                        <Translate
                                            className="cmarket__btn-text"
                                            content="crowdmarket.buy-btn"
                                            component="span"
                                        />
                                    </Link>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}
export default CrowdMarket;
