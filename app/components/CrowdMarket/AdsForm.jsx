import React from "react";
import Translate from "react-translate-component";
import AccountStore from "stores/AccountStore";
import {ChainStore} from "bitsharesjs";
import counterpart from "counterpart";
import {connect} from "alt-react";
import Icon from "../Icon/Icon";

class AdsForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentAccount: AccountStore.getState().currentAccount,
            imgResult: false
        };
    }

    fileLoadIndicator() {
        this.setState({
            imgResult: true
        });
    }

    render() {
        let cats = {
            cat01: counterpart.translate("crowdmarket.cat-01"),
            cat02: counterpart.translate("crowdmarket.cat-02"),
            cat03: counterpart.translate("crowdmarket.cat-03"),
            cat04: counterpart.translate("crowdmarket.cat-04"),
            cat05: counterpart.translate("crowdmarket.cat-05"),
            cat06: counterpart.translate("crowdmarket.cat-06"),
            cat07: counterpart.translate("crowdmarket.cat-07"),
            cat08: counterpart.translate("crowdmarket.cat-08"),
            cat09: counterpart.translate("crowdmarket.cat-09"),
            cat10: counterpart.translate("crowdmarket.cat-10")
        };

        return (
            <form
                className="new-board"
                action="https://crowdwiz.biz/board_add/api"
                method="POST"
                encType="multipart/form-data"
            >
                <div className="new-board__wrap">
                    <div className="new-board__center">
                        <input
                            className="new-board__field"
                            id="userAdTitle"
                            type="hidden"
                            name="name"
                            value={this.state.currentAccount}
                            required
                        />
                        <label
                            className="new-board__field-wrap"
                            htmlFor="userAdTitle"
                        >
                            <Translate
                                className="new-board__subtitle"
                                content="crowdmarket.set-title"
                            />

                            <input
                                className="new-board__field"
                                id="userAdTitle"
                                type="text"
                                name="title"
                                required
                            />
                        </label>
                        <label
                            className="new-board__field-wrap"
                            htmlFor="userAdDescription"
                        >
                            <Translate
                                className="new-board__subtitle"
                                content="crowdmarket.set-description"
                            />
                            <textarea
                                className="new-board__field"
                                id="userAdDescription"
                                rows="10"
                                name="description"
                                required
                            />
                        </label>
                        <label
                            className="new-board__field-wrap"
                            htmlFor="userAdPrice"
                        >
                            <Translate
                                className="new-board__subtitle"
                                content="crowdmarket.set-price"
                            />

                            <input
                                className="new-board__field"
                                id="userAdPrice"
                                type="number"
                                min="1"
                                name="price"
                                required
                            />
                        </label>
                        <label
                            className="new-board__field-wrap"
                            htmlFor="userAdategory"
                        >
                            <Translate
                                className="new-board__subtitle"
                                content="crowdmarket.set-category"
                            />
                            <select
                                className="new-board__field new-board__field--select"
                                id="userAdategory"
                                type="text"
                                name="category"
                                required
                            >
                                {Object.entries(cats).map(([cat, cat_name]) => (
                                    <option key={cat} value={cat}>
                                        {cat_name}
                                    </option>
                                ))}
                            </select>
                        </label>
                        {/* upload field */}
                        <div className="new-board__field-wrap">
                            <Translate
                                className="new-board__subtitle"
                                content="crowdmarket.set-img"
                                element="div"
                            />
                            <Translate
                                className="new-board__subtitle new-board__subtitle--text"
                                content="crowdmarket.set-img-text"
                                element="div"
                            />
                            <label
                                className="new-board__upload-label"
                                htmlFor="userAdImg"
                            >
                                <Icon size="3x" name="upload" />
                                {this.state.imgResult ? (
                                    <Translate
                                        className="cwd-upload__label-text"
                                        content="crowdmarket.file-ok"
                                        element="span"
                                    />
                                ) : (
                                    <Translate
                                        className="cwd-upload__label-text"
                                        content="crowdmarket.choose-file"
                                        element="span"
                                    />
                                )}
                            </label>
                            <input
                                className="new-board__upload"
                                id="userAdImg"
                                type="file"
                                name="img_url"
                                download
                                required
                                onChange={this.fileLoadIndicator.bind(this)}
                            />
                        </div>
                        <label
                            className="new-board__field-wrap"
                            htmlFor="userAdContacts"
                        >
                            <Translate
                                className="new-board__subtitle"
                                content="crowdmarket.set-contacts"
                            />

                            <input
                                className="new-board__field"
                                id="userAdContacts"
                                type="text"
                                name="contacts"
                                required
                            />
                        </label>
                    </div>
                    <button
                        className="board__buy-btn board__buy-btn--add-new"
                        type="submit"
                    >
                        <Translate content="crowdmarket.new-ad" />
                    </button>
                </div>
            </form>
        );
    }
}
export default (AdsForm = connect(
    AdsForm,
    {
        listenTo() {
            return [AccountStore];
        },
        getProps() {
            if (AccountStore.getState().passwordAccount) {
                return {
                    account: ChainStore.fetchFullAccount(
                        AccountStore.getState().passwordAccount
                    ),
                    currentAccount: AccountStore.getState().currentAccount
                };
            }
        }
    }
));
