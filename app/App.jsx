import React from "react";
import {ChainStore} from "bitsharesjs";
import AccountStore from "stores/AccountStore";
import NotificationStore from "stores/NotificationStore";
import {withRouter} from "react-router-dom";
import SyncError from "./components/SyncError";
import LoadingIndicator from "./components/LoadingIndicator";
// import BrowserNotifications from "./components/BrowserNotifications/BrowserNotificationsContainer";
import Header from "components/Layout/Header";
import ReactTooltip from "react-tooltip";
import NotificationSystem from "react-notification-system";
import TransactionConfirm from "./components/Blockchain/TransactionConfirm";
import WalletUnlockModal from "./components/Wallet/WalletUnlockModal";
import Deprecate from "./Deprecate";
import Incognito from "./components/Layout/Incognito";
import {isIncognito} from "feature_detect";
import {updateGatewayBackers} from "common/gatewayUtils";
import titleUtils from "common/titleUtils";
import {BodyClassName, Notification} from "crowdwiz-ui-modal";
import {DEFAULT_NOTIFICATION_DURATION} from "services/Notification";
import Loadable from "react-loadable";
import Borrow from "./components/Showcases/Borrow";
import LeftMenu from "./components/LeftMenu/LeftMenu";
import Landing from "./components/Landing/Landing";
import {Route, Switch, Redirect} from "react-router-dom";
import Page404 from "./components/Page404/Page404";

const Exchange = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "exchange" */ "./components/Exchange/ExchangeContainer"
        ),
    loading: LoadingIndicator
});

const BlocksContainer = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "explorer" */ "./components/Explorer/BlocksContainer"
        ),
    loading: LoadingIndicator
});

const Witnesses = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "witnesses" */ "./components/Explorer/Witnesses"
        ),
    loading: LoadingIndicator
});

const AccountPage = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "account" */ "./components/Account/AccountPage"
        ),
    loading: LoadingIndicator
});

const AccountDepositWithdraw = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "deposit-withdraw" */ "./components/Account/AccountDepositWithdraw"
        ),
    loading: LoadingIndicator
});

const Settings = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "settings" */ "./components/Settings/SettingsContainer"
        ),
    loading: LoadingIndicator
});

const Asset = Loadable({
    loader: () =>
        import(/* webpackChunkName: "asset" */ "./components/Blockchain/Asset"),
    loading: LoadingIndicator
});

const Block = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "block" */ "./components/Blockchain/BlockContainer"
        ),
    loading: LoadingIndicator
});

const CreateWorker = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "create-worker" */ "./components/Account/CreateWorker"
        ),
    loading: LoadingIndicator
});

/* GameZone */

const GameZone = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "gamezone" */ "./components/GameZone/GameZone"
        ),
    loading: LoadingIndicator
});

const GameHeads = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "gamezoneHeads" */ "./components/GameZone/GameHeads"
        ),
    loading: LoadingIndicator
});

const GameScoop = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "GameScoop" */ "./components/GameZone/GameScoop"
        ),
    loading: LoadingIndicator
});

const GameMatrix = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "GameMatrix" */ "./components/GameZone/Matrix/GameMatrix"
        ),
    loading: LoadingIndicator
});

const GameMatrixDev = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "GameMatrixDev" */ "./components/GameZone/Matrix/GameMatrixDev"
        ),
    loading: LoadingIndicator
});

/* CrowdMarket */

const CrowdMarket = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "crowdmarket" */ "./components/CrowdMarket/CrowdMarket"
        ),
    loading: LoadingIndicator
});

const CrowdAds = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "CrowdAds" */ "./components/CrowdMarket/CrowdAds"
        ),
    loading: LoadingIndicator
});

/*CrowdGateway IndexPage*/
const GatewayIndexPage = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "GatewayIndexPage" */ "./components/CrowdGateway/GatewayIndexPage"
        ),
    loading: LoadingIndicator
});

/*CrowdGateway Decentralized*/
const CrowdGateway = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "CrowdGatewayDEC" */ "./components/CrowdGateway/CrowdGateway"
        ),
    loading: LoadingIndicator
});

/*CrowdGateway BTC*/
const GatewayBTC = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "GatewayBTC" */ "./components/CrowdGateway/Gateways/GatewayBTC"
        ),
    loading: LoadingIndicator
});

/*Investment*/
const Investment = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "Investment" */ "./components/Investment/Investment"
        ),
    loading: LoadingIndicator
});

/*DexIndex*/
const DexIndex = Loadable({
    loader: () =>
        import(
            /* webpackChunkName: "DexIndex" */ "./components/Exchange/DexIndex"
        ),
    loading: LoadingIndicator
});

import LoginSelector from "./components/LoginSelector";
import {CreateWalletFromBrainkey} from "./components/Wallet/WalletCreate";
import PriceAlertNotifications from "./components/PriceAlertNotifications";

class App extends React.Component {
    constructor() {
        super();

        let syncFail =
            ChainStore.subError &&
            ChainStore.subError.message ===
                "ChainStore sync error, please check your system clock"
                ? true
                : false;
        this.state = {
            loading: false,
            synced: this._syncStatus(),
            syncFail,
            incognito: false,
            incognitoWarningDismissed: false,
            height: window && window.innerHeight
        };

        this._rebuildTooltips = this._rebuildTooltips.bind(this);
        this._chainStoreSub = this._chainStoreSub.bind(this);
        this._syncStatus = this._syncStatus.bind(this);
        this._getWindowHeight = this._getWindowHeight.bind(this);

        Notification.config({
            duration: DEFAULT_NOTIFICATION_DURATION,
            top: 90
        });
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this._getWindowHeight);
        NotificationStore.unlisten(this._onNotificationChange);
        ChainStore.unsubscribe(this._chainStoreSub);
        clearInterval(this.syncCheckInterval);
    }

    /**
     * Returns the current blocktime, or exception if not yet available
     * @returns {Date}
     */
    getBlockTime() {
        let dynGlobalObject = ChainStore.getObject("2.1.0");
        if (dynGlobalObject) {
            let block_time = dynGlobalObject.get("time");
            if (!/Z$/.test(block_time)) {
                block_time += "Z";
            }
            return new Date(block_time);
        } else {
            throw new Error("Blocktime not available right now");
        }
    }

    /**
     * Returns the delta between the current time and the block time in seconds, or -1 if block time not available yet
     *
     * Note: Could be integrating properly with BlockchainStore to send out updates, but not necessary atp
     */
    getBlockTimeDelta() {
        try {
            let bt =
                (this.getBlockTime().getTime() +
                    ChainStore.getEstimatedChainTimeOffset()) /
                1000;
            let now = new Date().getTime() / 1000;
            return Math.abs(now - bt);
        } catch (err) {
            return -1;
        }
    }

    _syncStatus(setState = false) {
        let synced = this.getBlockTimeDelta() < 5;
        if (setState && synced !== this.state.synced) {
            this.setState({synced});
        }
        return synced;
    }

    _setListeners() {
        try {
            window.addEventListener("resize", this._getWindowHeight, {
                capture: false,
                passive: true
            });
            NotificationStore.listen(this._onNotificationChange.bind(this));
            ChainStore.subscribe(this._chainStoreSub);
            AccountStore.tryToSetCurrentAccount();
        } catch (e) {
            console.error("e:", e);
        }
    }

    componentDidMount() {
        this._setListeners();
        this.syncCheckInterval = setInterval(
            this._syncStatus.bind(this, true),
            5000
        );

        this.props.history.listen(this._rebuildTooltips);

        this._rebuildTooltips();

        isIncognito(
            function(incognito) {
                this.setState({incognito});
            }.bind(this)
        );
        updateGatewayBackers();
    }

    componentDidUpdate(prevProps) {
        if (this.props.location !== prevProps.location) {
            this.onRouteChanged();
        }
    }

    onRouteChanged() {
        document.title = titleUtils.GetTitleByPath(
            this.props.location.pathname
        );
    }

    _onIgnoreIncognitoWarning() {
        this.setState({incognitoWarningDismissed: true});
    }

    _rebuildTooltips() {
        if (this.rebuildTimeout) return;
        ReactTooltip.hide();

        this.rebuildTimeout = setTimeout(() => {
            if (this.refs.tooltip) {
                this.refs.tooltip.globalRebuild();
            }
            this.rebuildTimeout = null;
        }, 1500);
    }

    _chainStoreSub() {
        let synced = this._syncStatus();
        if (synced !== this.state.synced) {
            this.setState({synced});
        }
        if (
            ChainStore.subscribed !== this.state.synced ||
            ChainStore.subError
        ) {
            let syncFail =
                ChainStore.subError &&
                ChainStore.subError.message ===
                    "ChainStore sync error, please check your system clock"
                    ? true
                    : false;
            this.setState({
                syncFail
            });
        }
    }

    /** Usage: NotificationActions.[success,error,warning,info] */
    _onNotificationChange() {
        let notification = NotificationStore.getState().notification;
        if (notification.autoDismiss === void 0) {
            notification.autoDismiss = 10;
        }
        if (this.refs.notificationSystem)
            this.refs.notificationSystem.addNotification(notification);
    }

    _getWindowHeight() {
        this.setState({height: window && window.innerHeight});
    }

    // /** Non-static, used by passing notificationSystem via react Component refs */
    // _addNotification(params) {
    //     console.log("add notification:", this.refs, params);
    //     this.refs.notificationSystem.addNotification(params);
    // }

    render() {
        let {incognito, incognitoWarningDismissed} = this.state;
        let {walletMode, theme, location, match, ...others} = this.props;
        let content = null;

        this.props.location.pathname;

        if (this.state.syncFail) {
            content = <SyncError />;
        } else if (this.state.loading) {
            content = (
                <div className="grid-frame vertical">
                    <LoadingIndicator
                        loadingText={"Connecting to APIs and starting app"}
                    />
                </div>
            );
        } else if (__DEPRECATED__) {
            content = <Deprecate {...this.props} />;
        } else {
            let accountName =
                AccountStore.getState().currentAccount ||
                AccountStore.getState().passwordAccount;
            accountName =
                accountName && accountName !== "null"
                    ? accountName
                    : "committee-account";
            content = (
                <div>
                    {this.props.location.pathname == "/" ? (
                        <div id="indexContent">
                            <Landing
                                history={this.props.history}
                                account={this.props.currentAccount}
                            />
                        </div>
                    ) : (
                        <div className="grid-frame vertical">
                            <div className="grid-block vertical">
                                <Header
                                    height={this.state.height}
                                    {...others}
                                />
                                <div id="mainContainer" className="grid-block">
                                    <div className="left-menu-container">
                                        <LeftMenu
                                            history={this.props.history}
                                        />
                                    </div>

                                    <div className="grid-block vertical">
                                        <Switch>
                                            <Route
                                                path="/create-account"
                                                component={LoginSelector}
                                            />
                                            <Route
                                                path="/account/:account_name"
                                                component={AccountPage}
                                                history={this.props.history}
                                            />
                                            <Route
                                                exact
                                                path="/markets-dashboard"
                                                component={DexIndex}
                                                history={this.props.history}
                                            />
                                            <Route
                                                path="/market/:marketID"
                                                component={Exchange}
                                            />
                                            <Route
                                                path="/settings/:tab"
                                                component={Settings}
                                            />
                                            <Route
                                                path="/settings"
                                                component={Settings}
                                            />

                                            {/* TODO: remove legacy gateways */}

                                            {/* <Route
                                                path="/deposit-withdraw"
                                                exact
                                                component={
                                                    AccountDepositWithdraw
                                                }
                                            /> */}

                                            <Redirect
                                                path={"/voting"}
                                                to={{
                                                    pathname: `/account/${accountName}/voting`
                                                }}
                                            />
                                            <Route
                                                path="/explorer"
                                                component={BlocksContainer}
                                            />
                                            <Route
                                                path="/witnesses"
                                                component={Witnesses}
                                            />
                                            <Route
                                                path="/asset/:symbol"
                                                component={Asset}
                                            />
                                            <Route
                                                exact
                                                path="/block/:height"
                                                component={Block}
                                            />
                                            <Route
                                                exact
                                                path="/block/:height/:txIndex"
                                                component={Block}
                                            />
                                            <Route
                                                path="/borrow"
                                                component={Borrow}
                                            />
                                            {/* Wallet backup/restore routes */}
                                            {/* <Route
                                                path="/wallet"
                                                component={WalletManager}
                                            />
                                            <Route
                                                path="/create-wallet-brainkey"
                                                component={
                                                    CreateWalletFromBrainkey
                                                }
                                            /> */}
                                            <Route
                                                path="/create-worker"
                                                component={CreateWorker}
                                            />
                                            <Route
                                                exact
                                                path="/gamezone"
                                                component={GameZone}
                                            />
                                            <Route
                                                exact
                                                path="/gamezone/heads-or-tails"
                                                component={GameHeads}
                                                history={this.props.history}
                                                synced={this.state.synced}
                                            />
                                            <Route
                                                exact
                                                path="/gamezone/scoop"
                                                component={GameScoop}
                                                account={
                                                    this.props.currentAccount
                                                }
                                            />
                                            <Route
                                                exact
                                                path="/gamezone/matrix-game"
                                                component={GameMatrix}
                                                account={
                                                    this.props.currentAccount
                                                }
                                            />
                                            <Route
                                                exact
                                                path="/gamezone/matrix-game/:account_name"
                                                component={GameMatrixDev}
                                            />
                                            <Route
                                                exact
                                                path="/crowdmarket"
                                                component={CrowdMarket}
                                                account={
                                                    this.props.currentAccount
                                                }
                                            />
                                            <Route
                                                exact
                                                path="/crowdmarket/ads-board"
                                                component={CrowdAds}
                                                account={
                                                    this.props.currentAccount
                                                }
                                            />
                                            <Route
                                                exact
                                                path="/gateway"
                                                component={GatewayIndexPage}
                                                account={
                                                    this.props.currentAccount
                                                }
                                            />
                                            <Route
                                                exact
                                                path="/gateway/dex"
                                                component={CrowdGateway}
                                            />
                                            <Route
                                                exact
                                                path="/gateway/btc"
                                                component={GatewayBTC}
                                            />
                                            <Route
                                                exact
                                                path="/investment"
                                                component={Investment}
                                                history={this.props.history}
                                            />
                                            <Route
                                                path="*"
                                                component={Page404}
                                            />
                                        </Switch>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )}
                    <ReactTooltip
                        ref="tooltip"
                        place="top"
                        type={theme === "lightTheme" ? "dark" : "light"}
                        effect="solid"
                    />
                </div>
            );
        }

        let deviceWidth = window.innerWidth;
        let notificationWidth;

        if (deviceWidth > 375) {
            notificationWidth = "375px";
        } else {
            notificationWidth = "340px";
        }

        return (
            <div
                style={{backgroundColor: !theme ? "#262626" : null}}
                className={theme}
            >
                <BodyClassName className={theme}>
                    {walletMode && incognito && !incognitoWarningDismissed ? (
                        <Incognito
                            onClickIgnore={this._onIgnoreIncognitoWarning.bind(
                                this
                            )}
                        />
                    ) : null}
                    <div id="content-wrapper">
                        {content}
                        <NotificationSystem
                            ref="notificationSystem"
                            allowHTML={true}
                            style={{
                                Containers: {
                                    DefaultStyle: {
                                        width: notificationWidth
                                    }
                                }
                            }}
                        />
                        <TransactionConfirm />
                        {/* <BrowserNotifications /> */}
                        <PriceAlertNotifications />
                        <WalletUnlockModal />
                    </div>
                </BodyClassName>
            </div>
        );
    }
}

export default withRouter(App);
